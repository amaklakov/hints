package by.ait.backend.validation;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.ResponseConst;
import by.ait.backend.repository.ExamRepository;
import by.ait.backend.repository.SubjectRepository;
import by.ait.backend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserFromDb {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private SubjectRepository subjectRepository;

    public String getTaskOwner(String taskId){

        try {
            return taskRepository.findTaskByTaskId(taskId).getCreatedBy();
        }
        catch (NullPointerException e){
            return "0";
        }
    }
    public String getExamOwner(String examId){

        try {
            return examRepository.findExamByExamId(examId).getCreatedBy();
        }
        catch (NullPointerException e){
            return "0";
        }
    }
    public String getSubjectOwner(String subjectId){

        try {
            return subjectRepository.findSubjectBySubjectId(subjectId).getCreatedBy();
        }
        catch (NullPointerException e){
            return "0";
        }
    }

    public static DefaultReturn getTaskNotFound(){
        return new DefaultReturn<>(ResponseConst.TASK_NOT_FOUND.errorId, ResponseConst.TASK_NOT_FOUND.errorMessageRu, null);
    }
    public static DefaultReturn getExamNotFound(){
        return new DefaultReturn<>(ResponseConst.EXAM_NOT_FOUND.errorId, ResponseConst.EXAM_NOT_FOUND.errorMessageRu, null);
    }
    public static DefaultReturn getSubjectNotFound(){
        return new DefaultReturn<>(ResponseConst.SUBJECT_NOT_FOUND.errorId, ResponseConst.SUBJECT_NOT_FOUND.errorMessageRu, null);
    }
}
