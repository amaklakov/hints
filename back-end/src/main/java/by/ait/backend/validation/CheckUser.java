package by.ait.backend.validation;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.AuthorizationCode;
import by.ait.backend.constants.ValidationConst;
import by.ait.backend.model.User;
import by.ait.backend.repository.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckUser {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ValidationConst validationConst;

    public int checkUserToLogin(User user) {

        User userFromDb = userRepository.findUserByEmail(user.getEmail());

        if (userFromDb != null) {
            if (userFromDb.isBlocked()) {
                return AuthorizationCode.BLOCKED;
            }

            String pass = DigestUtils.sha1Hex(user.getPassword());

            if (pass.equals(userFromDb.getPassword())) {
                System.out.println("Login: SUCCESS");
                return AuthorizationCode.SUCCESS;
            }
        }
        return AuthorizationCode.INCORRECT_EMAIL_OR_PASSWORD;
    }


    public int checkUserToRegistrable(User user) {

        //TODO regExp [A-Za-z0-9_]+
        //TODO перевести на русский сообщения об ошибках

        User userEmailFromDb = userRepository.findUserByEmail(user.getEmail());
        User userNameFromDb = userRepository.findUserByUserName(user.getUserName());

        //Имя уже занято
        if (userNameFromDb != null) {
            return AuthorizationCode.INCORRECT_NAME;
        }

        //Почта уже занята
        if (userEmailFromDb != null) {
            return AuthorizationCode.INCORRECT_EMAIL_OR_PASSWORD;
        }
        user.setPassword(DigestUtils.sha1Hex(user.getPassword()));
        return AuthorizationCode.SUCCESS;
    }
}
