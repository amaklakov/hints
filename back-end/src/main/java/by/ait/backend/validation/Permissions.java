package by.ait.backend.validation;

import by.ait.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Permissions {

    @Autowired
    private UserRepository userRepository;

    public boolean checkUserPermissions(String userId, String ownerId) {
        int userRole = userRepository.findUserByUserId(userId).getUserRole();
        return userId.equals(ownerId) || userRole == 0;
    }
}
