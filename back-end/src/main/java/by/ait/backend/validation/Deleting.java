package by.ait.backend.validation;

import by.ait.backend.model.Exam;
import by.ait.backend.repository.ExamRepository;
import by.ait.backend.repository.SubjectRepository;
import by.ait.backend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Deleting {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private SubjectRepository subjectRepository;

    public void deleteAllBySubjectId(String subjectId){
        List<Exam> examArray = examRepository.findExamsBySubjectId(subjectId);
        for (Exam exam : examArray){
            deleteTasksByExamId(exam.getExamId());
        }
        subjectRepository.deleteSubjectBySubjectId(subjectId);
        System.out.println("Subject удален успешно");
    }

    public void deleteTasksByExamId(String examId){
        taskRepository.deleteTasksByExamId(examId);
        examRepository.deleteExamByExamId(examId);
        System.out.println("Таски и экзамен удалены успешно");
    }
}
