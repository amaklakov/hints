package by.ait.backend.validation;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.ResponseConst;
import by.ait.backend.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionCheck {

    @Autowired
    private SessionRepository sessionRepository;

    public String findSession(String sessionToken){
        try {
            System.out.println("find session " + sessionToken);
            return sessionRepository.findSessionBySessionToken(sessionToken).getUserId();
        }
        catch (NullPointerException e){
            return "0";
        }
    }

    public static DefaultReturn getSessionNotFound(){
        return new DefaultReturn<>(ResponseConst.SESSION_NOT_FOUND.errorId, ResponseConst.SESSION_NOT_FOUND.errorMessageRu, null);
    }
}
