package by.ait.backend.util;

import java.util.Date;

public class Util {

    public static String getCurrentDate(){
        return String.valueOf((new Date()).getTime());
    }
}
