package by.ait.backend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "sessions")
public class Session {

    @Id
    private String userId;

    @NotBlank
    private String sessionToken;

    private String createdAt;

    @Override
    public String toString() {
        return "Session{" +
                "userId='" + userId + '\'' +
                ", sessionToken='" + sessionToken + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }

    public Session() {
    }

    public Session(String userId, @NotBlank String sessionToken, String createdAt) {
        this.userId = userId;
        this.sessionToken = sessionToken;
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
