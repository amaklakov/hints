package by.ait.backend.controller;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.MappingConst;
import by.ait.backend.constants.RequestConst;
import by.ait.backend.constants.ResponseConst;
import by.ait.backend.constants.ValidationConst;
import by.ait.backend.model.Task;
import by.ait.backend.repository.TaskRepository;
import by.ait.backend.response.TasksWithFavorite;
import by.ait.backend.util.Util;
import by.ait.backend.validation.Permissions;
import by.ait.backend.validation.SessionCheck;
import by.ait.backend.validation.UserFromDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private SessionCheck sessionCheck;
    @Autowired
    private UserFromDb userFromDb;
    @Autowired
    private Permissions permissions;
    @Autowired
    private ValidationConst validationConst;

    @PostMapping(MappingConst.GET_TASK)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn getTask(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Task task) {

        System.out.println("---/api/getTask ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        try {
            System.out.println(task.getTaskId());
            Task taskFromDb = taskRepository.findTaskByTaskId(task.getTaskId());
            System.out.println(taskFromDb);
            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, taskFromDb);
        } catch (NullPointerException e) {
            return UserFromDb.getTaskNotFound();
        }

    }

    @GetMapping(MappingConst.GET_TASKS_BY_EXAM_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn getTasksByExamId(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestParam String examId) {

        System.out.println("---/api/getTasksByExamId ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        try {
            List<Task> taskArray = taskRepository.findTasksByExamId(examId);
            List<Task> favoriteTasks = new ArrayList<>();
            List<Task> notFavoriteTasks = new ArrayList<>();

            for (Task task : taskArray) {
                if (task.isFavorite()) {
                    favoriteTasks.add(task);
                } else {
                    notFavoriteTasks.add(task);
                }
            }
            TasksWithFavorite tasksWithFavorite = new TasksWithFavorite(favoriteTasks, notFavoriteTasks);

            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, tasksWithFavorite);
        } catch (NullPointerException e) {
            return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
        }
    }

    @PostMapping(MappingConst.CREATE_TASK)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn createTask(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Task newTask) {

        System.out.println("---/api/createTask ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        if (!validationConst.validate(newTask.getTaskName(), ValidationConst.TASK_NAME)) {
            return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
        }
        if (!validationConst.validate(newTask.getContent(), ValidationConst.TASK_CONTENT)) {
            return new DefaultReturn<>(ResponseConst.INVALID_CONTENT.errorId, ResponseConst.INVALID_CONTENT.errorMessageRu, null);
        }

        newTask.setTaskId(Util.getCurrentDate());
        newTask.setCreatedAt(Util.getCurrentDate());
        newTask.setCreatedBy(userId);
        taskRepository.save(newTask);
        System.out.println("Успех");
        return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);

    }

    @PostMapping(MappingConst.MARK_FAVORITE_TASK_BY_TASK_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn markTaskFavorite(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Task markedTask) {

        System.out.println("---/api/markTaskFavorite ---");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getTaskOwner(markedTask.getTaskId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if (ownerId.equals("0")) {
            return UserFromDb.getTaskNotFound();
        }

        Task taskFromDb = taskRepository.findTaskByTaskId(markedTask.getTaskId());

        if (permissions.checkUserPermissions(userId, ownerId)) {
            taskFromDb.setFavorite(markedTask.isFavorite());
            taskRepository.save(taskFromDb);
            System.out.println("Task " + taskFromDb.getTaskName() + " c Id " + taskFromDb.getTaskId() + " был помечен как favorite");
            if (markedTask.isFavorite()) {
                return new DefaultReturn<>(ResponseConst.SUCCESS_FAVORITE_TASK.errorId, ResponseConst.SUCCESS_FAVORITE_TASK.errorMessageRu, null);
            }
            return new DefaultReturn<>(ResponseConst.SUCCESS_UNFAVORITE_TASK.errorId, ResponseConst.SUCCESS_UNFAVORITE_TASK.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);


    }

    @PostMapping(MappingConst.UPDATE_TASK_BY_TASK_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn updateTask(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Task updatedTask) {

        System.out.println("---/api/updateTask ---");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getTaskOwner(updatedTask.getTaskId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if (ownerId.equals("0")) {
            return UserFromDb.getTaskNotFound();
        }

        Task taskFromDb = taskRepository.findTaskByTaskId(updatedTask.getTaskId());

        if (permissions.checkUserPermissions(userId, ownerId)) {
            if (!validationConst.validate(updatedTask.getTaskName(), ValidationConst.TASK_NAME)) {
                return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
            }

            if (updatedTask.getContent() != null) {
                if (!validationConst.validate(updatedTask.getContent(), ValidationConst.TASK_CONTENT)) {
                    return new DefaultReturn<>(ResponseConst.INVALID_CONTENT.errorId, ResponseConst.INVALID_CONTENT.errorMessageRu, null);
                }
                System.out.println("Описание есть, меняю");
                taskFromDb.setContent(updatedTask.getContent());
            }

            taskFromDb.setTaskName(updatedTask.getTaskName());
            taskFromDb.setUpdatedAt(Util.getCurrentDate());
            taskFromDb.setUpdatedBy(userId);
            taskRepository.save(taskFromDb);
            System.out.println("Task " + taskFromDb.getTaskName() + " c Id " + taskFromDb.getTaskId() + " был успешно обновлен");

            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
    }

    @PostMapping(MappingConst.DELETE_TASK_BY_TASK_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn deleteTask(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Task taskToDelete) {

        System.out.println("---/api/deleteTask ---");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getTaskOwner(taskToDelete.getTaskId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if (ownerId.equals("0")) {
            return UserFromDb.getTaskNotFound();
        }

        if (permissions.checkUserPermissions(userId, ownerId)) {
            System.out.println("Успех");
            System.out.println("Айди таска который удаляем - " + taskToDelete.getTaskId());
            taskRepository.deleteTaskByTaskId(taskToDelete.getTaskId());

            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
    }

}
