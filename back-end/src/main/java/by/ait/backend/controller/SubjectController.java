package by.ait.backend.controller;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.ResponseConst;
import by.ait.backend.constants.MappingConst;
import by.ait.backend.constants.RequestConst;
import by.ait.backend.constants.ValidationConst;
import by.ait.backend.model.Subject;
import by.ait.backend.repository.SubjectRepository;
import by.ait.backend.util.Util;
import by.ait.backend.validation.Deleting;
import by.ait.backend.validation.Permissions;
import by.ait.backend.validation.SessionCheck;
import by.ait.backend.validation.UserFromDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class SubjectController {

    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private SessionCheck sessionCheck;
    @Autowired
    private UserFromDb userFromDb;
    @Autowired
    private Permissions permissions;
    @Autowired
    private ValidationConst validationConst;
    @Autowired
    private Deleting deleting;


    @GetMapping(MappingConst.GET_SUBJECTS)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn getSubjects(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken) {

        System.out.println("---/api/getSubjects ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        try {
            System.out.println("getSubjects");
            //TODO Сделать для Одмена вывод вообще всех сабджектов
            return new DefaultReturn<List>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, subjectRepository.findSubjectsByCreatedBy(userId));
        }
        catch (NullPointerException e){
            return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
        }

    }

    @PostMapping(MappingConst.CREATE_SUBJECT)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn createSubject(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Subject newSubject) {
        System.out.println("--- /api/createSubject ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        if (!validationConst.validate(newSubject.getSubjectName(), ValidationConst.SUBJECT_NAME)) {
            return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
        }

        newSubject.setCreatedBy(userId);
        newSubject.setSubjectId(Util.getCurrentDate());
        subjectRepository.save(newSubject);
        System.out.println("Успех");
        return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
    }

    @PostMapping(MappingConst.UPDATE_SUBJECT_BY_SUBJECT_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn updateSubject(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Subject updatedSubject) {

        System.out.println("---/api/updateSubject ---");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getSubjectOwner(updatedSubject.getSubjectId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if(ownerId.equals("0")) {
            return UserFromDb.getSubjectNotFound();
        }
        Subject subjectFromDb = subjectRepository.findSubjectBySubjectId(updatedSubject.getSubjectId());

        if (permissions.checkUserPermissions(userId, ownerId)) {
            if (!validationConst.validate(updatedSubject.getSubjectName(), ValidationConst.SUBJECT_NAME)){
                return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
            }

            subjectFromDb.setSubjectName(updatedSubject.getSubjectName());
            subjectFromDb.setUpdatedAt(Util.getCurrentDate());
            subjectFromDb.setUpdatedBy(userId);
            subjectRepository.save(subjectFromDb);

            System.out.println("Subject " + subjectFromDb.getSubjectName() + " c Id " + subjectFromDb.getSubjectId() + " был успешно обновлен");
            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
    }

    @PostMapping(MappingConst.DELETE_SUBJECT_BY_SUBJECT_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn deleteSubject(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Subject subjectToDelete) {

        System.out.println("---/api/deleteSubject ---");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getSubjectOwner(subjectToDelete.getSubjectId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if(ownerId.equals("0")) {
            return UserFromDb.getSubjectNotFound();
        }
        if (permissions.checkUserPermissions(userId, ownerId)) {

            deleting.deleteAllBySubjectId(subjectToDelete.getSubjectId());
            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
    }
}