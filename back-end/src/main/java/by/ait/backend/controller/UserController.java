package by.ait.backend.controller;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.*;
import by.ait.backend.model.Session;
import by.ait.backend.model.User;
import by.ait.backend.repository.SessionRepository;
import by.ait.backend.repository.UserRepository;
import by.ait.backend.response.SessionWithRole;
import by.ait.backend.util.Util;
import by.ait.backend.validation.CheckUser;
import by.ait.backend.validation.SessionCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private SessionCheck sessionCheck;
    @Autowired
    private CheckUser checkUser;
    @Autowired
    private ValidationConst validationConst;

    @PostMapping(MappingConst.LOGOUT)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn logout(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken) {

        System.out.println("  ---api/Logout--- ");
        String userId = sessionCheck.findSession(sessionToken);
        System.out.println("userId - " + userId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        sessionRepository.deleteSessionBySessionToken(sessionToken);
        System.out.println("LogOut: SUCCESS");
        return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
    }

    /**
     * 1. validation (outside) -> true || false if false return errors
     * валидация в случае false выдает назад errorId
     * 2. 1 == true? create user
     * 3. return
     */
    @PostMapping(MappingConst.LOGIN)
    @CrossOrigin(origins = "http://localhost:4200")

    public DefaultReturn login(@Valid @RequestBody User user) {

        if (!validationConst.validate(user.getEmail(), ValidationConst.EMAIL)) {
            return new DefaultReturn<>(ResponseConst.INVALID_EMAIL.errorId, ResponseConst.INVALID_EMAIL.errorMessageRu, null);
        }
        if (!validationConst.validate(user.getPassword(), ValidationConst.PASSWORD)) {
            return new DefaultReturn<>(ResponseConst.INVALID_PASSWORD.errorId, ResponseConst.INVALID_PASSWORD.errorMessageRu, null);
        }

        switch (checkUser.checkUserToLogin(user)) {
            case AuthorizationCode.SUCCESS:
                User userFromDb = userRepository.findUserByEmail(user.getEmail());

                // refactor все проверки не тут
                Session sessionFromDb = sessionRepository.findSessionByUserId(userFromDb.getUserId());
                if (sessionFromDb != null) {
                    System.out.println("Delete pri povtore proizoshel");
                    sessionRepository.delete(sessionFromDb);
                }


                Session newSession = new Session(userFromDb.getUserId(), Util.getCurrentDate(), Util.getCurrentDate());
                sessionRepository.save(newSession);

                SessionWithRole sessionWithRole = new SessionWithRole(newSession.getSessionToken(), userFromDb.getUserRole());
                return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, sessionWithRole);

            case AuthorizationCode.BLOCKED:
                return new DefaultReturn<>(ResponseConst.ACCOUNT_BLOCKED.errorId, ResponseConst.ACCOUNT_BLOCKED.errorMessageRu, null);
            case AuthorizationCode.INCORRECT_EMAIL_OR_PASSWORD:
                return new DefaultReturn<>(ResponseConst.INCORRECT_DATA.errorId, ResponseConst.INCORRECT_DATA.errorMessageRu, null);
        }
        return null;
    }

    @PostMapping(MappingConst.REGISTRATION)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn registration(@Valid @RequestBody User newUser) {

        if (!validationConst.validate(newUser.getEmail(), ValidationConst.EMAIL)) {
            return new DefaultReturn<>(ResponseConst.INVALID_EMAIL.errorId, ResponseConst.INVALID_EMAIL.errorMessageRu, null);
        }
        if (!validationConst.validate(newUser.getPassword(), ValidationConst.PASSWORD)) {
            return new DefaultReturn<>(ResponseConst.INVALID_PASSWORD.errorId, ResponseConst.INVALID_PASSWORD.errorMessageRu, null);
        }
        if (!validationConst.validate(newUser.getUserName(), ValidationConst.USER_NAME)) {
            return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
        }

        switch (checkUser.checkUserToRegistrable(newUser)) {
            case AuthorizationCode.SUCCESS:
                newUser.setUserId(Util.getCurrentDate());
                newUser.setUserRole(1);
                newUser.setBlocked(false);
                newUser.setCreatedAt(Util.getCurrentDate());
                userRepository.save(newUser);
                return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
            case AuthorizationCode.INCORRECT_EMAIL_OR_PASSWORD:
                return new DefaultReturn<>(ResponseConst.INCORRECT_DATA.errorId, ResponseConst.INCORRECT_DATA.errorMessageRu, null);
            case AuthorizationCode.INCORRECT_NAME:
                return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
        }
        return null;
    }
}
