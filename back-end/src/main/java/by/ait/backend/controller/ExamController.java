package by.ait.backend.controller;

import by.ait.backend.DefaultReturn;
import by.ait.backend.constants.*;
import by.ait.backend.model.Exam;
import by.ait.backend.model.Task;
import by.ait.backend.repository.ExamRepository;
import by.ait.backend.util.Util;
import by.ait.backend.validation.Deleting;
import by.ait.backend.validation.Permissions;
import by.ait.backend.validation.SessionCheck;
import by.ait.backend.validation.UserFromDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ExamController {

    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private SessionCheck sessionCheck;
    @Autowired
    private UserFromDb userFromDb;
    @Autowired
    private Permissions permissions;
    @Autowired
    private ValidationConst validationConst;
    @Autowired
    private Deleting deleting;

    @GetMapping(MappingConst.GET_EXAMS_BY_SUBJECT_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn getExamsBySubjectId(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestParam String subjectId) {

        System.out.println("---/api/getExamsBySubjectId ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        try {
            System.out.println("getExamsBySubjectId" + examRepository.findExamsBySubjectId(subjectId));
            return new DefaultReturn<List>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageEn, examRepository.findExamsBySubjectId(subjectId));
        }
        catch (NullPointerException e){
            //TODO Вместо null должен приходить пустой массив
            return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
        }
    }

    @PostMapping(MappingConst.CREATE_EXAM)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn createExam(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Exam newExam) {

        System.out.println("--- /api/createExam ---");
        String userId = sessionCheck.findSession(sessionToken);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }

        if (!validationConst.validate(newExam.getExamName(), ValidationConst.EXAM_NAME)){
            return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
        }
        if (!validationConst.validate(newExam.getExamDescription(), ValidationConst.EXAM_DESCRIPTION)){
            return new DefaultReturn<>(ResponseConst.INVALID_DESCRIPTION.errorId, ResponseConst.INVALID_DESCRIPTION.errorMessageRu, null);
        }

        newExam.setExamId(Util.getCurrentDate());
        newExam.setCreatedAt(Util.getCurrentDate());
        newExam.setCreatedBy(userId);
        examRepository.save(newExam);
        System.out.println("Успех");
        return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);

    }

    @PostMapping(MappingConst.DELETE_EXAM_BY_EXAM_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn deleteExam(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Exam examToDelete) {

        System.out.println("---/api/deleteExam ---");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getExamOwner(examToDelete.getExamId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if(ownerId.equals("0")) {
            return UserFromDb.getExamNotFound();
        }
        if (permissions.checkUserPermissions(userId, ownerId)) {

            deleting.deleteTasksByExamId(examToDelete.getExamId());

            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
    }

    @PostMapping(MappingConst.UPDATE_EXAM_BY_EXAM_ID)
    @CrossOrigin(origins = "http://localhost:4200")
    public DefaultReturn updateExam(@Valid @RequestHeader(RequestConst.SESSION_TOKEN) String sessionToken, @RequestBody Exam updatedExam) {

        System.out.println(" --- updateExam --- ");
        String userId = sessionCheck.findSession(sessionToken);
        String ownerId = userFromDb.getExamOwner(updatedExam.getExamId());
        System.out.println("userId - " + userId + ", ownerId - " + ownerId);

        if (userId.equals("0")) {
            return SessionCheck.getSessionNotFound();
        }
        if(ownerId.equals("0")) {
            return UserFromDb.getExamNotFound();
        }

        Exam examFromDb = examRepository.findExamByExamId(updatedExam.getExamId());

        if (permissions.checkUserPermissions(userId, ownerId)) {
            if (!validationConst.validate(updatedExam.getExamName(), ValidationConst.EXAM_NAME)){
                return new DefaultReturn<>(ResponseConst.INVALID_NAME.errorId, ResponseConst.INVALID_NAME.errorMessageRu, null);
            }

            if (updatedExam.getExamDescription() != null) {
                if (!validationConst.validate(updatedExam.getExamDescription(), ValidationConst.EXAM_DESCRIPTION)){
                    return new DefaultReturn<>(ResponseConst.INVALID_DESCRIPTION.errorId, ResponseConst.INVALID_DESCRIPTION.errorMessageRu, null);
                }

                System.out.println("Описание есть, меняю");
                examFromDb.setExamDescription(updatedExam.getExamDescription());
            }

            examFromDb.setExamName(updatedExam.getExamName());
            examFromDb.setUpdatedAt(Util.getCurrentDate());
            examFromDb.setUpdatedBy(userId);
            examRepository.save(examFromDb);

            System.out.println("Exam " + examFromDb.getExamName() + " c Id " + examFromDb.getExamId() + " был успешно обновлен");
            return new DefaultReturn<>(ResponseConst.SUCCESS.errorId, ResponseConst.SUCCESS.errorMessageRu, null);
        }
        return new DefaultReturn<>(ResponseConst.NO_PERMISSIONS.errorId, ResponseConst.NO_PERMISSIONS.errorMessageRu, null);
    }

}
