package by.ait.backend.response;

public class SessionWithRole {

    private String sessionToken;

    private int userRole;

    @Override
    public String toString() {
        return "SessionWithRole{" +
                "sessionToken='" + sessionToken + '\'' +
                ", userRole='" + userRole + '\'' +
                '}';
    }

    public SessionWithRole(String sessionToken, int userRole) {
        this.sessionToken = sessionToken;
        this.userRole = userRole;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }
}
