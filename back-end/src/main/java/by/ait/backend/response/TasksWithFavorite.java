package by.ait.backend.response;

import by.ait.backend.model.Task;

import java.util.List;

public class TasksWithFavorite {

    private List<Task> favoriteTasks;

    private List<Task> notFavoriteTasks;

    @Override
    public String toString() {
        return "TasksWithFavorite{" +
                "favoriteTasks=" + favoriteTasks +
                ", notFavoriteTasks=" + notFavoriteTasks +
                '}';
    }

    public TasksWithFavorite(List<Task> favoriteTasks, List<Task> notFavoriteTasks) {
        this.favoriteTasks = favoriteTasks;
        this.notFavoriteTasks = notFavoriteTasks;
    }

    public List<Task> getFavoriteTasks() {
        return favoriteTasks;
    }

    public void setFavoriteTasks(List<Task> favoriteTasks) {
        this.favoriteTasks = favoriteTasks;
    }

    public List<Task> getNotFavoriteTasks() {
        return notFavoriteTasks;
    }

    public void setNotFavoriteTasks(List<Task> notFavoriteTasks) {
        this.notFavoriteTasks = notFavoriteTasks;
    }
}
