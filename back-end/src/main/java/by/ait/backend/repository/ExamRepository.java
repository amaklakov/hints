package by.ait.backend.repository;

import by.ait.backend.model.Exam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ExamRepository extends JpaRepository<Exam, String> {
    List<Exam> findExamsBySubjectId(String subjectId);
    Exam findExamByExamId(String examId);

    @Transactional
    void deleteExamByExamId(String examId);
}
