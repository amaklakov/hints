package by.ait.backend.repository;

import by.ait.backend.model.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {
   List<Subject> findSubjectsByCreatedBy(String createdBy);
    Subject findSubjectBySubjectId(String subjectId);

    @Transactional
    void deleteSubjectBySubjectId(String subjectId);
}
