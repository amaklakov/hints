package by.ait.backend.repository;

import by.ait.backend.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Repository
public interface SessionRepository extends JpaRepository<Session, String> {

    Session findSessionBySessionToken(String sessionToken);
    Session findSessionByUserId(String userId);
    @Transactional
    void deleteSessionBySessionToken(String sessionToken);
}
