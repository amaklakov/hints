package by.ait.backend.repository;

import by.ait.backend.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository <Task, String> {
    Task findTaskByTaskId(String taskId);
    List<Task> findTasksByExamId(String examId);

    @Transactional
    void deleteTaskByTaskId(String taskId);
    @Transactional
    void deleteTasksByExamId(String examId);

}
