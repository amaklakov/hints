package by.ait.backend.repository;


import by.ait.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findUserByEmail(String Email);
    User findUserByUserName(String userName);
    User findUserByUserId(String userId);
}
