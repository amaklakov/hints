package by.ait.backend;

import by.ait.backend.response.ErrorWithMessage;

public class DefaultReturn<T> extends ErrorWithMessage {

    private T content;
    // refactor Читать про "Темплэйты в Java"


    public DefaultReturn(int id, String message, T content) {
        super(id, message);
        this.content = content;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}