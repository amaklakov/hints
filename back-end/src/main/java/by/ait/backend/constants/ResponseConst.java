package by.ait.backend.constants;

public class ResponseConst {

    public final static MessageType SUCCESS = new MessageType(0, "Ок", "Ok");
    public final static MessageType SUCCESS_FAVORITE_TASK = new MessageType(0, "Успешно добавлено в избранное", "Add to favorite");
    public final static MessageType SUCCESS_UNFAVORITE_TASK = new MessageType(0, "Успешно удалено из избранного", "Delete from favorite");

    public final static MessageType TASK_NOT_FOUND = new MessageType(1, "Вопрос не найден", "Task not found");
    public final static MessageType EXAM_NOT_FOUND = new MessageType(1, "Экзамен не найден", "Exam not found");
    public final static MessageType SUBJECT_NOT_FOUND = new MessageType(1, "Предмет не найден", "Subject not found");
    public final static MessageType SESSION_NOT_FOUND = new MessageType(1, "Сессия не найдена", "Session not found");

    public final static MessageType NO_PERMISSIONS = new MessageType(2, "У вас недостаточно прав", "You don't have enough permissions");

    public final static MessageType ACCOUNT_BLOCKED = new MessageType(3, "Аккаунт заблокирован", "Account is blocked");
    public final static MessageType INCORRECT_DATA = new MessageType(3, "Неверный Email или пароль", "Incorrect Email or password");

    public final static MessageType INVALID_EMAIL = new MessageType(4, "Введенный Email не соответствует требованиям", "Invalid Email");
    public final static MessageType INVALID_PASSWORD = new MessageType(4, "Введенный пароль не соответствует требованиям", "Invalid password");
    public final static MessageType INVALID_NAME = new MessageType(4, "Введенное имя не соответствует требованиям", "Invalid name");
    public final static MessageType INVALID_DESCRIPTION = new MessageType(4, "Введенное описание экзамена не соответствует требованиям", "Invalid description of exam");
    public final static MessageType INVALID_CONTENT = new MessageType(4, "Введеное описание вопроса не соответствует требованиям", "Invalid content of task");

}
