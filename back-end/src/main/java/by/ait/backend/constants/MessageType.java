package by.ait.backend.constants;

public class MessageType {

    public int errorId;
    public String errorMessageRu;
    public String errorMessageEn;

    public MessageType(int errorId, String errorMessageRu, String errorMessageEn) {
        this.errorId = errorId;
        this.errorMessageRu = errorMessageRu;
        this.errorMessageEn = errorMessageEn;
    }
}
