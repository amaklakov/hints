package by.ait.backend.constants;

import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ValidationConst {

    public static final LengthType EMAIL = new LengthType("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$", 8, 50);
    public static final LengthType USER_NAME = new LengthType("^[a-zA-Z0-9_]{4,30}$", 3, 30);
    public static final LengthType PASSWORD = new LengthType("^[a-zA-Z0-9_+-=%]{8,30}$", 8, 30);

    public static final LengthType SUBJECT_NAME = new LengthType(null, 3, 50);

    public static final LengthType EXAM_NAME = new LengthType(null, 3, 150);
    public static final LengthType EXAM_DESCRIPTION = new LengthType(null, 3, 350);

    public static final LengthType TASK_NAME = new LengthType(null, 3, 150);
    public static final LengthType TASK_CONTENT = new LengthType(null, 3, 1000);


    public Boolean validate(String str, LengthType currentData){
        if(currentData.pattern != null){
            if(!check(str, currentData.pattern)){
                return false;
            }
        }
        if (currentData.minLength >=1){
            if (str.length()<currentData.minLength){
                return false;
            }
        }
        if (currentData.maxLength != 0){
            if (str.length()>currentData.maxLength){
                return false;
            }
        }
        return true;
    }


    private static boolean check (String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
}
