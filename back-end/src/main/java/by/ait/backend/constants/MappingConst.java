package by.ait.backend.constants;

public class MappingConst {

    public final static String GET_TASK  = "api/getTask";
    public final static String GET_TASKS_BY_EXAM_ID  = "api/getTasksByExamId";
    public final static String CREATE_TASK = "/api/createTask";
    public final static String DELETE_TASK_BY_TASK_ID = "/api/deleteTask";
    public final static String UPDATE_TASK_BY_TASK_ID = "/api/updateTask";
    public final static String MARK_FAVORITE_TASK_BY_TASK_ID = "/api/markFavorite";

    public final static String GET_EXAMS_BY_SUBJECT_ID  = "api/getExamsBySubjectId";
    public final static String CREATE_EXAM = "/api/createExam";
    public final static String DELETE_EXAM_BY_EXAM_ID = "/api/deleteExam";
    public final static String UPDATE_EXAM_BY_EXAM_ID = "/api/updateExam";

    public final static String GET_SUBJECTS  = "/api/getSubjects";
    public final static String CREATE_SUBJECT = "/api/createSubject";
    public final static String DELETE_SUBJECT_BY_SUBJECT_ID = "/api/deleteSubject";
    public final static String UPDATE_SUBJECT_BY_SUBJECT_ID = "/api/updateSubject";

    public final static String REGISTRATION  = "/api/registration";
    public final static String LOGIN = "/api/login";
    public final static String LOGOUT = "/api/logout";

}
