package by.ait.backend.constants;

public class LengthType {

    public String pattern;
    public int minLength;
    public int maxLength;


    public LengthType(String pattern, int minLength, int maxLength) {
        this.pattern = pattern;
        this.minLength = minLength;
        this.maxLength = maxLength;
    }
}
