package by.ait.backend.constants;

public class AuthorizationCode {

    public final static int SUCCESS = 0;
    public final static int BLOCKED = 1;
    public final static int INCORRECT_EMAIL_OR_PASSWORD = 2;
    public final static int INCORRECT_NAME = 3;

}
