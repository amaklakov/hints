import {Directive, ElementRef, HostListener, NgModule} from '@angular/core';

@Directive({selector: '[no-new-line]'})
export class NoNewLineDirective {
	constructor(public el: ElementRef) {
	}

	@HostListener('keydown', ['$event'])
	onEnterPressed(event: KeyboardEvent) {
		if (event.target === this.el.nativeElement && event.keyCode === 13) {
			event.preventDefault();
			// console.log('ENTER: \'' + this.el.nativeElement.value + '\'');
		}
	}
}

@NgModule({
	imports: [],
	exports: [NoNewLineDirective],
	declarations: [NoNewLineDirective],
	providers: []
})
export class NoNewLineDirectiveModule {
}


