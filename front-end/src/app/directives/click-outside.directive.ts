import {Directive, ElementRef, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';

@Directive({selector: '[click-outside]'})
export class ClickOutsideDirective implements OnDestroy {

	@Input('click-outside')
	public set listenClickSub(value: boolean) {
		if (value) {
			setTimeout(() => {
				this.listenClick();
			}, 500)
		} else {
			this.clickUnsubscribe();
		}
	}

	@Output('onClickOutside')
	public onClickOutside: EventEmitter<any> = new EventEmitter<any>();

	private clickListenSub: Subscription;
	private touchListenSub: Subscription;

	constructor(private elRef: ElementRef) {

	}

	public listenClick() {
		this.clickListenSub = fromEvent(document, 'click').subscribe((event: Event) => {
			const clickedInside = this.elRef.nativeElement.contains(event.target);
			if (!clickedInside) {
				this.onClickOutside.emit(true);
				this.clickUnsubscribe();
			}
		});

		// на мобилке
		this.touchListenSub = fromEvent(document, 'touchstart').subscribe((event: Event) => {
			if (!this.elRef.nativeElement.contains(event.target)) {
				let touchEndSub = fromEvent(document, 'touchend').subscribe((event: Event) => {
					if (!this.elRef.nativeElement.contains(event.target)) {
						this.onClickOutside.emit(true);
						touchEndSub.unsubscribe();
						this.clickUnsubscribe();
					}
				});
			}
		});
	}

	private clickUnsubscribe(): void {
		if (this.clickListenSub) {
			this.clickListenSub.unsubscribe();
		}

		if (this.touchListenSub) {
			this.touchListenSub.unsubscribe();
		}
	}

	ngOnDestroy() {
		this.clickUnsubscribe();
	}
}
