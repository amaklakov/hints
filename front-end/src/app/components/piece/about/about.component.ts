import {Component, OnInit} from '@angular/core';
import {Developer} from './developer.interface';
import {ApplicationService} from '../../../services/application.service';

@Component({
	selector: 'app-about',
	templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {

	public developers: Array<Developer>;

	constructor(private appService: ApplicationService) {
	}

	ngOnInit() {
		this.developers = [
			{
				name: 'Артём Маклаков',
				pic: '../assets/img/artyom.jpg',
				description: 'Главный front-end разработчик',
				link: 'https://vk.com/mav0901'
			},
			{
				name: 'Тим Серединский',
				pic: '../assets/img/tim.jpg',
				description: 'Главный back-end разработчик',
				link: 'https://vk.com/id381816424'
			}
		];
	}

	public goToDeveloper(person: Developer): void {
		if (person.link) {
			this.appService.openLinkInNewTab(person.link);
		}
	}

}
