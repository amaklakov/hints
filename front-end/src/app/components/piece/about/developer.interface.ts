export interface Developer {
	name: string;
	description: string;
	pic: string;
	link?: string;
}
