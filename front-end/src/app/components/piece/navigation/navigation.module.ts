import {NgModule} from '@angular/core';
import {NavigationComponent} from './navigation.component';
import {SharedModule} from '../../../shared.module';

@NgModule({
	imports: [
		SharedModule
	],
	exports: [NavigationComponent],
	declarations: [NavigationComponent]
})
export class NavigationModule {
}
