import {Component, EventEmitter, Output} from '@angular/core';
import {ApplicationService} from '../../../services/application.service';
import {RoutingConstants} from '../../../constants/routing.constants';

@Component({
	selector: 'nav-component',
	templateUrl: './navigation.component.html'
})
export class NavigationComponent {

	@Output('onItemClicked')
	public onItemClicked: EventEmitter<any> = new EventEmitter<any>();

	constructor(private appService: ApplicationService) {
	}

	public goToHome(): void {
		this.appService.appData.isLogged ?
			this.appService.goToPage(RoutingConstants.WORK_PLACE) :
			this.appService.goToPage(RoutingConstants.LOGIN);
		this.emitOnClick();
	}

	public goToNews(): void {
		this.appService.goToPage(RoutingConstants.NEWS);
		this.emitOnClick();
	}

	public goToAbout(): void {
		this.appService.goToPage(RoutingConstants.ABOUT);
		this.emitOnClick();
	}

	private emitOnClick(): void {
		this.onItemClicked.emit({isClicked: true});
	}
}
