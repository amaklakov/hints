import {NgModule} from '@angular/core';
import {LoginComponent} from './login.component';
import {SharedModule} from '../../../shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginService} from './login.service';
import {FormWarningModule} from '../../common/form-warning/form-warning.module';
import {ModalModule} from '../../common/modal/modal.module';

@NgModule({
	imports: [
		SharedModule,
		ReactiveFormsModule,
		FormWarningModule,

		ModalModule
	],
	exports: [LoginComponent],
	declarations: [LoginComponent],
	providers: [LoginService]
})
export class LoginModule {
}
