import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../services/http-client.service';
import {BackEndConstants} from '../../../constants/back-end.constants';

@Injectable()
export class LoginService {

	constructor(private httpClient: HttpClientService) {
	}

	public login(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.LOGIN, req, successCallback);
	}
}
