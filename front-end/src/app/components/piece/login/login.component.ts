import {Component} from '@angular/core';
import {ApplicationService} from '../../../services/application.service';
import {RoutingConstants} from '../../../constants/routing.constants';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {LocalStorageService} from '../../../services/local-storage.service';
import {isPresent} from '../../../util/util';
import {ModalService} from '../../common/modal/modal.service';
import {ValidationConstants} from '../../../constants/validation.constants';

@Component({
	selector: 'login',
	templateUrl: 'login.component.html'
})
export class LoginComponent {

	public loginForm: FormGroup;

	constructor(private appService: ApplicationService,
	            private fb: FormBuilder,
	            private loginService: LoginService,
	            private localStorageService: LocalStorageService,
	            private modalService: ModalService) {

		this.loginForm = fb.group({
			email: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.EMAIL_MINLENGTH),
				Validators.maxLength(ValidationConstants.EMAIL_MAXLENGTH),
				Validators.pattern(ValidationConstants.EMAIL_PATTERN)]),
			password: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.PASSWORD_MINLENGTH),
				Validators.maxLength(ValidationConstants.PASSWORD_MAXLENGTH),
				Validators.pattern(ValidationConstants.PASSWORD_PATTERN)])
		});
	}

	public login(): void {
		if (this.loginForm.valid) {
			this.loginService.login(this.loginForm.getRawValue(), (res) => {
				if (isPresent(res)) {

					if (res.errorId !== 0) {
						return this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
					}

					this.localStorageService.setItem(RoutingConstants.SESSION_TOKEN, res.content.sessionToken);
					this.localStorageService.setItem(RoutingConstants.USER_ROLE, res.content.userRole);

					this.appService.appData.isLogged = true;

					this.goToMainPage();
				}
			});

			// Uncomment if back is off
			// this.localStorageService.setItem(RoutingConstants.SESSION_TOKEN, '---------');
			// this.localStorageService.setItem(RoutingConstants.USER_ROLE, '0');
			// this.appService.appData.isLogged = true;
			// this.goToMainPage();
		} else {
			this.modalService.notify('warning', 'MODAL.FORM_NOT_VALID');
		}
	}

	private goToMainPage(): void {
		this.appService.goToPage(RoutingConstants.WORK_PLACE);
	}

	public register(): void {
		// switch ((new Date()).getTime() % 3) {
		//     case 0:
		//         this.modalService.notify('error', 'Ошибка! Не дам тебе зарегаться', 'И никогда больше не дам. Я тут сделаю много текста чтобы потом этот текст на 2 строчечки был или более чтобы показать что все хорошо у меня с переносм словцов красных!');
		//         break;
		//     case 1:
		//         this.modalService.notify('warning', 'Внимание! Невнимание!', 'гыгыгыггыгы', 0);
		//         break;
		//     case 2:
		//         this.modalService.notify('notification', 'Ура!');
		//         break;
		// }
		this.appService.goToPage(RoutingConstants.REGISTER);
	}
}
