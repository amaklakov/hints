import {NgModule} from '@angular/core';
import {NewsComponent} from './news.component';
import {SharedModule} from '../../../shared.module';

@NgModule({
	imports: [
		SharedModule
	],
	declarations: [NewsComponent],
	exports: [NewsComponent]
})
export class NewsModule {
}
