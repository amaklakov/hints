export interface NewsPiece {
	id: string;             // ID новости,
	createdAt: string;      // дата создания новости,

	header: string;         // заголовок новости,
	text?: string;          // текст новости,
	author?: string;        // автор сообщения
}
