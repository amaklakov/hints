import {Component, OnInit} from '@angular/core';
import {NewsPiece} from './news-piece.interface';

@Component({
	selector: 'app-news',
	templateUrl: './news.component.html'
})
export class NewsComponent implements OnInit {
	private readonly HEADER_LENGTH: number = 125;
	private readonly TEXT_LENGTH: number = 300;
	private readonly AUTHOR_LENGTH: number = 100;

	public news: Array<NewsPiece>;

	constructor() {
	}

	ngOnInit() {
		this.news = [
			{
				id: '1',
				header: 'Скоро будет релиз!',
				text: 'А может и нет. Но будем стараться',
				createdAt: '18.07.2018',
				author: 'Маклаков Артем'
			},
			{
				id: '2',
				header: 'Скоро будет переведено в режим тестирования!',
				text: 'Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов Много слов ',
				createdAt: '17.07.2018',
				author: 'Тимошка Серединский'
			},
			{
				id: '3',
				header: '',
				text: 'Дааа!',
				createdAt: '16.07.2018',
				author: ''
			}
		];

		if (this.news) {
			this.news.map((item) => {
				if (item.header && item.header.length > this.HEADER_LENGTH) {
					item.header = item.header.slice(0, this.HEADER_LENGTH - 3) + '...';
				}

				if (item.text && item.text.length > this.TEXT_LENGTH) {
					item.text = item.text.slice(0, this.TEXT_LENGTH - 3) + '...';
				}

				if (item.author && item.author.length > this.AUTHOR_LENGTH) {
					item.author = item.author.slice(0, this.AUTHOR_LENGTH - 3) + '...';
				}

				if (item.createdAt) {
					item.createdAt;
				}

				return item;
			});
		}
	}

}
