import {Component} from '@angular/core';
import {RoutingConstants} from '../../../constants/routing.constants';
import {ApplicationService} from '../../../services/application.service';

@Component({
	selector: 'footer',
	templateUrl: 'footer.component.html'
})

export class FooterComponent {
	constructor(private appService: ApplicationService) {
	}

	public goToVk(): void {
		this.appService.openLinkInNewTab(RoutingConstants.VK);
	}

	public goToTelegram(): void {
		this.appService.openLinkInNewTab(RoutingConstants.TELEGRAM);
	}

	public goToYouTube(): void {
		this.appService.openLinkInNewTab(RoutingConstants.YOU_TUBE);
	}
}
