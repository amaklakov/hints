import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LocalStorageService} from '../../../services/local-storage.service';
import {ApplicationService} from '../../../services/application.service';
import {RoutingConstants} from '../../../constants/routing.constants';
import {HttpClientService} from '../../../services/http-client.service';
import {BackEndConstants} from '../../../constants/back-end.constants';
import {ModalService} from '../../common/modal/modal.service';

@Component({
	selector: 'header-component',
	templateUrl: 'header.component.html',
	styleUrls: ['header.component.scss']
})
export class HeaderComponent {

	public isDisabledCopy: boolean = true;
	public isOpenMobileMenu: boolean = false;

	// For language
	@Input() usingEn: boolean = false;
	@Output() usingEnChange: EventEmitter<any> = new EventEmitter<any>();

	constructor(private localStorageService: LocalStorageService,
	            private httpClient: HttpClientService,
	            public appService: ApplicationService,
	            public modalService: ModalService) {
	}

	/**
	 * Эмитим смену языка
	 */
	public changeLang(): void {
		this.usingEn = !this.usingEn;
		this.usingEnChange.emit({usingEn: this.usingEn});
	}

	/**
	 * Выход из приложения
	 */
	public exit(): void {
		this.httpClient.callPOST(BackEndConstants.LOGOUT,
			{sessionToken: this.localStorageService.getItem(RoutingConstants.SESSION_TOKEN)},
			(res) => {
				if (!res.errorId) {
					this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
					this.localStorageService.clear();
					this.appService.appData.isLogged = false;
					return this.goToLogin();
				}

				return this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			});
	}

	public goToLogin(): void {
		this.appService.goToPage(RoutingConstants.LOGIN);
	}

	public toggleMobileMenu(): void {
		this.isOpenMobileMenu = !this.isOpenMobileMenu;
		document.getElementsByClassName('content')[0].classList.toggle('-translate-right');
		document.getElementsByClassName('footer')[0].classList.toggle('-translate-right');
		document.getElementsByTagName('body')[0].style.position = this.isOpenMobileMenu ? 'fixed' : 'initial';
	}

	public clickOutsideMenu(event: any) {
		if (this.isOpenMobileMenu) {
			this.toggleMobileMenu();
		}
	}
}
