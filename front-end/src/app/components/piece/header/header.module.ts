import {NgModule} from '@angular/core';
import {HeaderComponent} from './header.component';
import {NavigationModule} from '../navigation/navigation.module';
import {SharedModule} from '../../../shared.module';
import {ClickOutsideDirectiveModule} from '../../../directives/directives.module';

@NgModule({
	imports: [SharedModule, NavigationModule, ClickOutsideDirectiveModule],
	exports: [HeaderComponent],
	declarations: [HeaderComponent],
	providers: []
})
export class HeaderModule {
}
