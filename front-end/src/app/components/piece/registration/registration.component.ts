import {Component} from '@angular/core';
import {ApplicationService} from '../../../services/application.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RoutingConstants} from '../../../constants/routing.constants';
import {RegistrationService} from './registration.service';
import {ModalService} from '../../common/modal/modal.service';
import {ValidationConstants} from '../../../constants/validation.constants';

@Component({
	selector: 'registration',
	templateUrl: './registration.component.html'
})
export class RegistrationComponent {
	public registrationForm: FormGroup;

	constructor(private appService: ApplicationService,
	            private fb: FormBuilder,
	            private service: RegistrationService,
	            private modalService: ModalService) {
		this.registrationForm = fb.group({
			userName: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.USERNAME_MINLENGTH),
				Validators.maxLength(ValidationConstants.USERNAME_MAXLENGTH),
				Validators.pattern(ValidationConstants.USERNAME_PATTERN)]),
			email: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.EMAIL_MINLENGTH),
				Validators.maxLength(ValidationConstants.EMAIL_MAXLENGTH),
				Validators.pattern(ValidationConstants.EMAIL_PATTERN)]),
			password: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.PASSWORD_MINLENGTH),
				Validators.maxLength(ValidationConstants.PASSWORD_MAXLENGTH),
				Validators.pattern(ValidationConstants.PASSWORD_PATTERN)]),
			passwordRepeat: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.PASSWORD_MINLENGTH),
				Validators.maxLength(ValidationConstants.PASSWORD_MAXLENGTH),
				Validators.pattern(ValidationConstants.PASSWORD_PATTERN)])
		});
	}

	public register(): void {
		let formValue = this.registrationForm.getRawValue();
		if (this.registrationForm.valid) {
			if (formValue.password === formValue.passwordRepeat) {
				return this.service.register(formValue, (res) => {
					this.goToLoginPage();
				});
			}

			return this.modalService.notify('warning', 'MODAL.PASSWORDS_NOT_EQUAL');
		}

		this.modalService.notify('warning', 'MODAL.FORM_NOT_VALID');
	}

	private goToLoginPage(): void {
		this.appService.goToPage(RoutingConstants.LOGIN);
	}

}
