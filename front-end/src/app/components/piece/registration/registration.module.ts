import {NgModule} from '@angular/core';
import {RegistrationComponent} from './registration.component';
import {SharedModule} from '../../../shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {RegistrationService} from './registration.service';
import {FormWarningModule} from '../../common/form-warning/form-warning.module';

@NgModule({
	imports: [
		SharedModule,
		ReactiveFormsModule,
		FormWarningModule
	],
	declarations: [RegistrationComponent],
	exports: [RegistrationComponent],
	providers: [RegistrationService]
})
export class RegistrationModule {
}
