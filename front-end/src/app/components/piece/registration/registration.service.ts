import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../services/http-client.service';
import {BackEndConstants} from '../../../constants/back-end.constants';

@Injectable()
export class RegistrationService {

	constructor(private httpClient: HttpClientService) {
	}

	public register(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.REGISTRATION, req, successCallback);
	}
}
