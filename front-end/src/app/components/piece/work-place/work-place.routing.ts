import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {WorkPlaceComponent} from './work-place.component';
import {RoutingConstants} from '../../../constants/routing.constants';
import {ExamComponent} from '../entity/exam/exam.component';
import {SubjectComponent} from '../entity/subject/subject.component';
import {TaskComponent} from '../entity/task/task.component';
import {QuestionComponent} from '../entity/question/question.component';
import {SubjectFormComponent} from '../entity/subject/subject-form/subject-form.component';
import {ExamFormComponent} from '../entity/exam/exam-form/exam-form.component';
import {TaskFormComponent} from '../entity/task/task-form/task-form.component';
import {AuthGuardService} from '../../../services/auth-guard.service';

const WorkPlaceRoutes: Routes = <Routes> [
	{
		path: '', component: WorkPlaceComponent,
		pathMatch: 'full',
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.SUBJECT,
		component: SubjectComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.SUBJECT_FORM,
		component: SubjectFormComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.EXAM,
		component: ExamComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.EXAM_FORM,
		component: ExamFormComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.TASK,
		component: TaskComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.TASK_FORM,
		component: TaskFormComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	},
	{
		path: RoutingConstants.QUESTION,
		component: QuestionComponent,
		canActivate: [AuthGuardService],
		data: {roles: [RoutingConstants.ROLE_ADMIN, RoutingConstants.ROLE_USER]}
	}
];

export const workPlaceRouting: ModuleWithProviders = RouterModule.forChild(WorkPlaceRoutes);


