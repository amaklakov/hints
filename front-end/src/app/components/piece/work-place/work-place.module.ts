import {NgModule} from '@angular/core';
import {WorkPlaceComponent} from './work-place.component';
import {SharedModule} from '../../../shared.module';
import {NavigationModule} from '../navigation/navigation.module';
import {TaskModule} from '../entity/task/task.module';
import {workPlaceRouting} from './work-place.routing';
import {SubjectModule} from '../entity/subject/subject.module';
import {QuestionModule} from '../entity/question/question.module';
import {ExamModule} from '../entity/exam/exam.module';
import {SubjectFormModule} from '../entity/subject/subject-form/subject-form.module';
import {ExamFormModule} from '../entity/exam/exam-form/exam-form.module';
import {TaskFormModule} from '../entity/task/task-form/task-form.module';

@NgModule({
	imports: [
		SharedModule,
		NavigationModule,
		SubjectModule,
		SubjectFormModule,
		ExamModule,
		ExamFormModule,
		TaskModule,
		TaskFormModule,
		QuestionModule,

		workPlaceRouting // роутинг
	],
	exports: [WorkPlaceComponent],
	declarations: [WorkPlaceComponent],
	providers: []
})
export class WorkPlaceModule {
}
