import {Component, OnInit} from '@angular/core';
import {ApplicationService} from '../../../services/application.service';
import {RoutingConstants} from '../../../constants/routing.constants';

@Component({
	selector: 'work-place',
	templateUrl: 'work-place.component.html'
})

export class WorkPlaceComponent implements OnInit {

	constructor(private appService: ApplicationService) {
	}

	ngOnInit() {
		// this.httpClient.callGET(BackEndConstants.GET_TASKS, res => {
		//     if (res) {
		//         this.notFavoriteTasks = res;
		//     }
		// });
	}

	public goToSubjectsPage() {
		this.appService.goToPage(RoutingConstants.SUBJECT_EXT);
	}
}
