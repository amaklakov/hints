import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../../services/http-client.service';
import {BackEndConstants} from '../../../../constants/back-end.constants';

@Injectable()
export class TaskService {

	constructor(private httpClient: HttpClientService) {
	}

	public getTasks(params: any, successCallback: Function): void {
		this.httpClient.callGET(BackEndConstants.GET_TASKS_BY_EXAM_ID, successCallback, params);
	}

	public createTask(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.CREATE_TASK, req, successCallback);
	}

	public updateTask(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.UPDATE_TASK, req, successCallback);
	}

	public deleteTask(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.DELETE_TASK, req, successCallback);
	}

	public markFavorite(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.MARK_FAVORITE, req, successCallback);
	}
}
