export class Task {
	examId: string;             // ID экзамена

	taskId: string;             // ID вопроса
	taskName: string;           // имя вопроса
	content?: string;           // описание вопроса
	isFavorite?: boolean;       // Избранное?

	createdAt: string;          // Дата создания
	createdBy: string;          // Кем создано (ID пользователя)
	updatedAt: string;          // Когда изменено
	updatedBy: string;          // Кем изменено
}
