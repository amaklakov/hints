import {NgModule} from '@angular/core';
import {TaskFormComponent} from './task-form.component';
import {SharedModule} from '../../../../../shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormWarningModule} from '../../../../common/form-warning/form-warning.module';

@NgModule({
	imports: [
		SharedModule,
		ReactiveFormsModule,
		FormWarningModule
	],
	declarations: [TaskFormComponent],
	exports: [TaskFormComponent]
})
export class TaskFormModule {
}
