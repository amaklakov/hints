import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RoutingConstants} from '../../../../../constants/routing.constants';
import {Subscription} from 'rxjs/index';
import {ApplicationService} from '../../../../../services/application.service';
import {TaskService} from '../task.service';
import {ModalService} from '../../../../common/modal/modal.service';
import {debounce} from 'rxjs/operators';
import {timer} from 'rxjs';
import {ValidationConstants} from '../../../../../constants/validation.constants';

@Component({
	selector: 'app-task-form',
	templateUrl: './task-form.component.html'
})
export class TaskFormComponent implements OnInit, OnDestroy {

	public routeParamsSub: Subscription;

	public createTaskForm: FormGroup;
	public createTaskFormSub: Subscription;

	public currentExamId: string;
	public taskId: string;
	public taskName: string;
	public content: string;
	public showUpdateButton: boolean = false;

	constructor(private service: TaskService,
	            private appService: ApplicationService,
	            private fb: FormBuilder,
	            private modalService: ModalService) {
		// создание формы
		this.createTaskForm = fb.group({
			taskName: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.TASK_NAME_MINLENGTH),
				Validators.maxLength(ValidationConstants.TASK_NAME_MAXLENGTH)]),
			content: new FormControl('', [
				Validators.minLength(ValidationConstants.CONTENT_MINLENGTH),
				Validators.maxLength(ValidationConstants.CONTENT_MAXLENGTH)]),
			examId: new FormControl('', [
				Validators.required])
		});
	}

	ngOnInit() {
		this.routeParamsSub = this.appService.getParams((params: any) => {
			this.currentExamId = params.examId;
			this.taskId = params.taskId;
			this.taskName = params.taskName;
			this.content = params.content;

			// когда есть currentExamId
			this.createTaskForm.patchValue({
				examId: this.currentExamId,
				taskName: this.taskName,
				content: this.content
			});
		});

		this.trimFormValue();
	}

	private trimFormValue(): void {
		this.createTaskFormSub = this.createTaskForm.valueChanges.pipe(debounce(() => timer(ValidationConstants.DEBOUNCE))).subscribe((value => {
			console.log('value', value);
			value.taskName = value.taskName ? value.taskName.trim() : value.taskName;
			value.content = value.content ? value.content.trim() : value.content;

			this.showUpdateButton = this.createTaskForm.valid && (this.taskName !== value.taskName || this.content !== value.content);
		}));
	}


	public createTask(): void {
		console.log(this.createTaskForm.getRawValue());
		if (this.createTaskForm.valid) {
			this.service.createTask(this.createTaskForm.getRawValue(), (res) => {
				if (!res.errorId) {
					this.goToTasksPage();
					return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
				}

				this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			});
		} else {
			this.modalService.notify('warning', 'MODAL.FORM_NOT_VALID');
		}
	}

	public updateTask(): void {
		if (this.createTaskForm.valid && (this.taskName !== this.createTaskForm.value.taskName || this.content !== this.createTaskForm.value.content)) {
			this.service.updateTask({
				examId: this.currentExamId,
				taskId: this.taskId,
				taskName: this.createTaskForm.value.taskName,
				content: this.createTaskForm.value.content
			}, (res) => {
				if (!res.errorId) {
					this.goToTasksPage();
					return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
				}

				this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			})
		} else {
			this.modalService.notify('warning', 'MODAL.NO_CHANGES');
		}
	}

	private goToTasksPage() {
		this.appService.goToPageWithParams(RoutingConstants.TASK_EXT, {examId: this.currentExamId});
	}

	ngOnDestroy() {
		this.routeParamsSub.unsubscribe();
		this.createTaskFormSub.unsubscribe();
	}

}
