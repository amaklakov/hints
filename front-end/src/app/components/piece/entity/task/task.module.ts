import {NgModule} from '@angular/core';
import {TaskComponent} from './task.component';
import {SharedModule} from '../../../../shared.module';
import {TaskService} from './task.service';

@NgModule({
	imports: [SharedModule],
	exports: [TaskComponent],
	declarations: [TaskComponent],
	providers: [TaskService]
})
export class TaskModule {
}
