import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {ApplicationService} from '../../../../services/application.service';
import {RoutingConstants} from '../../../../constants/routing.constants';
import {TaskService} from './task.service';
import {Task} from './task.model';
import {ModalService} from '../../../common/modal/modal.service';

@Component({
	selector: 'task',
	templateUrl: 'task.component.html'
})

export class TaskComponent implements OnInit, OnDestroy {

	public routeParamsSub: Subscription;
	public currentExamId: string;
	public notFavoriteTasks: Array<Task>;
	public favoriteTasks: Array<Task>;

	constructor(private appService: ApplicationService,
	            private service: TaskService,
	            private modalService: ModalService) {
	}

	ngOnInit() {
		this.routeParamsSub = this.appService.getParams((params: { examId: string }) => {
			this.currentExamId = params.examId;

			this.getTasks(this.currentExamId);
		});
	}

	public getTasks(examId: string): void {
		this.service.getTasks({examId: examId}, (res: any) => {
			this.notFavoriteTasks = res.content.notFavoriteTasks;
			this.favoriteTasks = res.content.favoriteTasks;
		});

		// this.notFavoriteTasks = [
		//     {
		//         examId: examId,
		//         taskId: '999',
		//         taskName: 'Теоремаghgghgkg ghg h k jk lh j h kj lhlh jkhhlhj Пифагора'
		//     },
		//     {
		//         examId: examId,
		//         taskId: '998',
		//         taskName: 'Теорема Богданович'
		//     },
		//     {
		//         examId: examId,
		//         taskId: '666',
		//         taskName: 'Теорема Метельской'
		//     }
		// ];
		//
		// this.favoriteTasks = [
		// 	{
		// 		examId: examId,
		// 		taskId: '999',
		// 		taskName: 'Теоремаghgghgkg ghg h k jk lh j h kj lhlh jkhhlhj Пифагора Пифагора'
		// 	},
		// 	{
		// 		examId: examId,
		// 		taskId: '998',
		// 		taskName: 'Теорема Богданович'
		// 	},
		// 	{
		// 		examId: examId,
		// 		taskId: '666',
		// 		taskName: 'Теорема Метельской'
		// 	}
		// ];
	}

	public createTask(): void {
		this.appService.goToPageWithParams(RoutingConstants.TASK_FORM_EXT, {examId: this.currentExamId});
	}

	public updateTask(task: Task): void {
		this.appService.goToPageWithParams(RoutingConstants.TASK_FORM_EXT, {
			examId: task.examId,
			taskId: task.taskId,
			taskName: task.taskName,
			content: task.content
		});
	}

	public deleteTask(task: Task): void {
		this.service.deleteTask({taskId: task.taskId}, (res) => {
			if (!res.errorId) {
				this.getTasks(this.currentExamId);
				return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
			}

			this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
		});
	}

	public goToTaskPage(task: Task): void {
		this.appService.goToPageWithParams(RoutingConstants.QUESTION_EXT, {taskId: task.taskId});
	}

	public makeFavorite(task: Task): void {
		if (task) {
			this.service.markFavorite({
				taskId: task.taskId,
				isFavorite: !task.isFavorite
			}, (res) => {
				if (res) {
					if (!res.errorId) {
						this.getTasks(this.currentExamId);
						return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
					}

					this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
				}
			})
		}
	}

	public uncheck(event: Event) {
		if (event && (<any>event.target).checked) {
			setTimeout(() => {
				(<any>event.target).checked = false;
			}, 5000)
		}
	}

	ngOnDestroy() {
		this.routeParamsSub.unsubscribe();
	}
}
