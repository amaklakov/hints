import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApplicationService} from '../../../../../services/application.service';
import {SubjectService} from '../subject.service';
import {RoutingConstants} from '../../../../../constants/routing.constants';
import {ModalService} from '../../../../common/modal/modal.service';
import {Subscription, timer} from 'rxjs';
import {debounce} from 'rxjs/operators';
import {ValidationConstants} from '../../../../../constants/validation.constants';

@Component({
	selector: 'subject-form',
	templateUrl: './subject-form.component.html'
})
export class SubjectFormComponent implements OnInit, OnDestroy {

	public routeParamsSub: Subscription;

	public createSubjectForm: FormGroup;
	public createSubjectFormSub: Subscription;

	public subjectName: string;
	public subjectId: string;
	public showUpdateButton: boolean = false;

	constructor(private service: SubjectService,
	            private appService: ApplicationService,
	            private modalService: ModalService,
	            private fb: FormBuilder) {
		// создание формы
		this.createSubjectForm = fb.group({
			subjectName: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.SUBJECT_NAME_MINLENGTH),
				Validators.maxLength(ValidationConstants.SUBJECT_NAME_MAXLENGTH)
			])
		});
	}

	ngOnInit() {
		this.routeParamsSub = this.appService.getParams((params) => {
			if (params && params.subjectId) {
				this.subjectId = params.subjectId;
				this.subjectName = params.subjectName;
				this.createSubjectForm.patchValue({subjectName: this.subjectName})
			}
		});

		this.trimFormValue();
	}

	private trimFormValue(): void {
		this.createSubjectFormSub = this.createSubjectForm.valueChanges.pipe(debounce(() => timer(ValidationConstants.DEBOUNCE))).subscribe((value => {
			value.subjectName = value.subjectName ? value.subjectName.trim() : value.subjectName;

			this.showUpdateButton = this.createSubjectForm.valid && this.subjectName !== value.subjectName;
		}));
	}

	public createSubject(): void {
		if (this.createSubjectForm.valid) {
			this.service.createSubject(this.createSubjectForm.getRawValue(), (res) => {
				if (!res.errorId) {
					this.goToSubjectPage();
					return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
				}
				this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			});
		} else {
			this.modalService.notify('warning', 'MODAL.FORM_NOT_VALID');
		}
	}

	public updateSubject(): void {
		if (this.createSubjectForm.valid && this.subjectId && this.subjectName !== this.createSubjectForm.getRawValue().subjectName) {
			return this.service.updateSubject({
				subjectId: this.subjectId,
				subjectName: this.createSubjectForm.getRawValue().subjectName
			}, (res) => {
				if (!res.errorId) {
					this.goToSubjectPage();
					return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
				}

				this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			})
		} else {
			this.modalService.notify('warning', 'MODAL.NO_CHANGES');
		}
	}

	private goToSubjectPage() {
		this.appService.goToPage(RoutingConstants.SUBJECT_EXT);
	}

	ngOnDestroy() {
		this.routeParamsSub.unsubscribe();
		this.createSubjectFormSub.unsubscribe();
	}

}
