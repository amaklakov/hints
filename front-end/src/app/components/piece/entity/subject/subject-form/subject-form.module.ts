import {NgModule} from '@angular/core';
import {SubjectFormComponent} from './subject-form.component';
import {SharedModule} from '../../../../../shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormWarningModule} from '../../../../common/form-warning/form-warning.module';

@NgModule({
	imports: [
		SharedModule,
		ReactiveFormsModule,
		FormWarningModule
	],
	declarations: [SubjectFormComponent],
	exports: [SubjectFormComponent]
})
export class SubjectFormModule {
}
