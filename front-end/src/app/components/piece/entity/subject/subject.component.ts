import {Component, OnInit} from '@angular/core';
import {SubjectService} from './subject.service';
import {Subject} from './subject.model';
import {ApplicationService} from '../../../../services/application.service';
import {RoutingConstants} from '../../../../constants/routing.constants';
import {ModalService} from '../../../common/modal/modal.service';

@Component({
	selector: 'subject',
	templateUrl: 'subject.component.html'
})

export class SubjectComponent implements OnInit {

	public subjects: Array<Subject>;

	constructor(private service: SubjectService,
	            private appService: ApplicationService,
	            private modalService: ModalService) {
	}

	ngOnInit() {
		this.getSubjects();
	}

	private getSubjects(): void {
		this.service.getSubjects((res: any) => {
			if (res.errorId !== 0) {
				return this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage)
			}

			this.subjects = res.content;
		});

		// this.subjects = [
		//     {
		//         groupId: '1',
		//         createdBy: '1',
		//         subjectId: '123',
		//         subjectName: 'История галимая'
		//     },
		//     {
		//         groupId: '0',
		// 	    createdBy: '2',
		//         subjectId: '1234',
		//         subjectName: 'Матешка'
		//     }
		// ];
	}

	public goToExamPage(subject: Subject): void {
		this.appService.goToPageWithParams(RoutingConstants.EXAM_EXT, {subjectId: subject.subjectId});
	}

	public addSubject(): void {
		this.appService.goToPage(RoutingConstants.SUBJECT_FORM_EXT);
	}

	updateSubject(subject: Subject): void {
		this.appService.goToPageWithParams(RoutingConstants.SUBJECT_FORM_EXT, {
			subjectId: subject.subjectId,
			subjectName: subject.subjectName
		});
	}

	public deleteSubject(subjectId: string, event: Event): void {
		event.stopPropagation();

		this.service.deleteSubject({subjectId: subjectId}, (res) => {
			if (!res.errorId) {
				this.getSubjects();
				return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
			}

			this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
		});
	}

	public uncheck(event: Event) {
		if (event && !(<any>event.target).checked) {
			setTimeout(() => {
				(<any>event.target).checked = false;
			}, 5000)
		}
	}
}
