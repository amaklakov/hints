export interface Subject {
	groupId?: string;           // ID группы
	subjectId?: string;         // ID предмета
	subjectName: string;        // имя предмета

	createdAt: string;          // Дата создания
	createdBy: string;          // Кем создано (ID пользователя)
	updatedAt: string;          // Когда изменено
	updatedBy: string;          // Кем изменено
}
