import {NgModule} from '@angular/core';

import {SubjectComponent} from './subject.component';
import {SharedModule} from '../../../../shared.module';
import {SubjectService} from './subject.service';
import {SubjectFormModule} from './subject-form/subject-form.module';

@NgModule({
	imports: [
		SharedModule,
		SubjectFormModule
	],
	exports: [SubjectComponent],
	declarations: [SubjectComponent],
	providers: [SubjectService]
})
export class SubjectModule {
}
