import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../../services/http-client.service';
import {BackEndConstants} from '../../../../constants/back-end.constants';

@Injectable()
export class SubjectService {

	constructor(private httpClient: HttpClientService) {
	}

	public getSubjects(successCallback: Function): void {
		this.httpClient.callGET(BackEndConstants.GET_SUBJECTS, successCallback);
	}

	public createSubject(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.CREATE_SUBJECT, req, successCallback);
	}

	public updateSubject(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.UPDATE_SUBJECT, req, successCallback);
	}

	public deleteSubject(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.DELETE_SUBJECT, req, successCallback);
	}
}
