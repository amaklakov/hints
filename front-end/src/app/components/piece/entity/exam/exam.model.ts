export interface Exam {
	subjectId: string;          // ID предмета, к которому относится экзамен

	examId: string;             // ID экзамена / кр
	examName: string;           // Название экзамена
	examDescription?: string;   // Описание экзамена

	createdAt: string;          // Дата создания
	createdBy: string;          // Кем создано (ID пользователя)
	updatedAt: string;          // Когда изменено
	updatedBy: string;          // Кем изменено
}
