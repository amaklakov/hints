import {NgModule} from '@angular/core';
import {ExamComponent} from './exam.component';
import {SharedModule} from '../../../../shared.module';
import {ExamService} from './exam.service';
import {ExamFormModule} from './exam-form/exam-form.module';

@NgModule({
	imports: [
		SharedModule,
		ExamFormModule
	],
	exports: [ExamComponent],
	declarations: [ExamComponent],
	providers: [ExamService]
})
export class ExamModule {
}
