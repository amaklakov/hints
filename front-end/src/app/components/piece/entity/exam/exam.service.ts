import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../../services/http-client.service';
import {BackEndConstants} from '../../../../constants/back-end.constants';

@Injectable()
export class ExamService {

	constructor(private httpClient: HttpClientService) {
	}

	public getExams(params: any, successCallback: Function): void {
		this.httpClient.callGET(BackEndConstants.GET_EXAMS_BY_SUBJECT_ID, successCallback, params);
	}

	public createExam(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.CREATE_EXAM, req, successCallback);
	}

	public updateExam(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.UPDATE_EXAM, req, successCallback);
	}

	public deleteExam(req: any, successCallback: Function): void {
		this.httpClient.callPOST(BackEndConstants.DELETE_EXAM, req, successCallback);
	}
}
