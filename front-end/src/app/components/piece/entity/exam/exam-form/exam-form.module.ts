import {NgModule} from '@angular/core';
import {ExamFormComponent} from './exam-form.component';
import {SharedModule} from '../../../../../shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormWarningModule} from '../../../../common/form-warning/form-warning.module';
import {NoNewLineDirectiveModule} from '../../../../../directives/no-new-line.directive';

@NgModule({
	imports: [
		SharedModule,
		ReactiveFormsModule,
		FormWarningModule,
		NoNewLineDirectiveModule
	],
	declarations: [ExamFormComponent],
	exports: [ExamFormComponent]
})
export class ExamFormModule {
}
