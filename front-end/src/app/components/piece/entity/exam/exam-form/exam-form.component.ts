import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RoutingConstants} from '../../../../../constants/routing.constants';
import {ApplicationService} from '../../../../../services/application.service';
import {Subscription, timer} from 'rxjs/index';
import {ExamService} from '../exam.service';
import {ModalService} from '../../../../common/modal/modal.service';
import {debounce} from 'rxjs/operators';
import {ValidationConstants} from '../../../../../constants/validation.constants';

@Component({
	selector: 'exam-form',
	templateUrl: './exam-form.component.html'
})
export class ExamFormComponent implements OnInit, OnDestroy {

	public routeParamsSub: Subscription;
	public currentSubjectId: string;

	public createExamForm: FormGroup;
	public createExamFormSub: Subscription;

	public examId: string;
	public examName: string;
	public examDescription: string;
	public showUpdateButton: boolean = false;

	constructor(private service: ExamService,
	            private appService: ApplicationService,
	            private fb: FormBuilder,
	            private modalService: ModalService) {
		// создание формы
		this.createExamForm = fb.group({
			examName: new FormControl('', [
				Validators.required,
				Validators.minLength(ValidationConstants.EXAM_NAME_MINLENGTH),
				Validators.maxLength(ValidationConstants.EXAM_NAME_MAXLENGTH)
			]),
			examDescription: new FormControl('', [
				Validators.minLength(ValidationConstants.EXAM_DESCRIPTION_MINLENGTH),
				Validators.maxLength(ValidationConstants.EXAM_DESCRIPTION_MAXLENGTH)
			]),
			subjectId: new FormControl('', [
				Validators.required
			])
		});
	}

	ngOnInit() {
		this.routeParamsSub = this.appService.getParams((params: any) => {
			this.currentSubjectId = params.subjectId;
			this.examId = params.examId;
			this.examName = params.examName;
			this.examDescription = params.examDescription;

			// когда есть subjectId
			this.createExamForm.patchValue({
				subjectId: this.currentSubjectId,
				examName: this.examName,
				examDescription: this.examDescription
			});
		});

		this.trimFormValue();
	}

	private trimFormValue(): void {
		this.createExamFormSub = this.createExamForm.valueChanges.pipe(debounce(() => timer(ValidationConstants.DEBOUNCE))).subscribe((value => {
			value.examName = value.examName ? value.examName.trim() : value.examName;
			value.examDescription = value.examDescription ? value.examDescription.trim() : value.examDescription;

			this.showUpdateButton = this.createExamForm.valid && (this.examName !== value.examName || this.examDescription !== value.examDescription);
		}));
	}

	public createExam(): void {
		if (this.createExamForm.valid) {
			this.service.createExam(this.createExamForm.getRawValue(), (res) => {
				if (!res.errorId) {
					this.goToExamPage();
					return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
				}
				this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			});
		} else {
			this.modalService.notify('warning', 'MODAL.FORM_NOT_VALID');
		}
	}

	public updateExam(): void {
		if (this.createExamForm.valid && (this.createExamForm.value.examName !== this.examName || this.createExamForm.value.examDescription !== this.examDescription)) {
			this.service.updateExam({
				subjectId: this.createExamForm.value.subjectId,
				examId: this.examId,
				examName: this.createExamForm.value.examName,
				examDescription: this.createExamForm.value.examDescription
			}, (res) => {
				if (!res.errorId) {
					this.goToExamPage();
					return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
				}

				this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
			});
		} else {
			this.modalService.notify('warning', 'MODAL.NO_CHANGES');
		}
	}

	private goToExamPage() {
		this.appService.goToPageWithParams(RoutingConstants.EXAM_EXT, {subjectId: this.currentSubjectId});
	}

	ngOnDestroy() {
		this.routeParamsSub.unsubscribe();
		this.createExamFormSub.unsubscribe();
	}

}
