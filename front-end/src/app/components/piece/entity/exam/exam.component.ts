import {Component, OnDestroy, OnInit} from '@angular/core';
import {Exam} from './exam.model';
import {Subscription} from 'rxjs/index';
import {ApplicationService} from '../../../../services/application.service';
import {ExamService} from './exam.service';
import {RoutingConstants} from '../../../../constants/routing.constants';
import {ModalService} from '../../../common/modal/modal.service';

@Component({
	selector: 'exam',
	templateUrl: 'exam.component.html'
})

export class ExamComponent implements OnInit, OnDestroy {

	public routeParamsSub: Subscription;
	public currentSubjectId: string;
	public exams: Array<Exam>;

	constructor(private appService: ApplicationService,
	            private service: ExamService,
	            private modalService: ModalService) {
	}

	ngOnInit() {
		this.routeParamsSub = this.appService.getParams((params: { subjectId: string }) => {
			this.currentSubjectId = params.subjectId;

			this.getExams(this.currentSubjectId);
		});
	}

	public getExams(subjectId: string): void {
		this.service.getExams({subjectId: subjectId}, (res: any) => {
			this.exams = res.content;
		});

		// this.exams = [
		//     {
		//         subjectId: subjectId,
		//         examId: '0001',
		//         examName: 'Экзамен рил жесткий',
		//         examDescription: 'Матан короче orem ipsum dolor sit amet, consectetur adipisicing elit. Beatae consequatur facere illo nemo non quasi, quod rem repellendus voluptatem! Ab, amet at blanditiis dolorum ea earum fugit iste iusto laborum molestias necessitatibus porro quam quia, quis veniam veritatis vitae? Aliquam at atque cum debitis dicta ea eos harum, ipsam labore magni modi nemo neque optio porro quam quisquam rem repudiandae sapiente sed soluta tempora tempore veritatis voluptas. Eos laborum quo tempora veritatis! Adipisci architecto, blanditiis deserunt dignissimos exercitationem in praesentium tempore. Ad cumque ea eius et exercitationem, facilis hic illum magni perspiciatis praesentium, provident quia reiciendis saepe, vel vitae. Adipisci delectus doloremque error exercitationem placeat praesentium rem. Adipisci debitis ea iste labore minus numquam ratione rem vitae, voluptates. Cupiditate dignissimos et exercitationem laboriosam, repellat repudiandae sint. Consequuntur corporis cupiditate ipsa modi molestiae sunt. Ab accusamus, aliquid aperiam consectetur deleniti eum exercitationem explicabo ipsa nam, nisi quo qu'
		//     },
		//     {
		//         subjectId: subjectId,
		//         examId: '0002',
		//         examName: 'Новый экзамен',
		//     },
		//     {
		//         subjectId: subjectId,
		//         examId: '0003',
		//         examName: 'Еще один',
		//         examDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae consequatur facere illo nemo non quasi, quod rem repellendus voluptatem! Ab, amet at blanditiis dolorum ea earum fugit iste iusto laborum molestias necessitatibus porro quam quia, quis veniam veritatis vitae? Aliquam at atque cum debitis dicta ea eos harum, ipsam labore magni modi nemo neque optio porro quam quisquam rem repudiandae sapiente sed soluta tempora tempore veritatis voluptas. Eos laborum quo tempora veritatis! Adipisci architecto, blanditiis deserunt dignissimos exercitationem in praesentium tempore. Ad cumque ea eius et exercitationem, facilis hic illum magni perspiciatis praesentium, provident quia reiciendis saepe, vel vitae. Adipisci delectus doloremque error exercitationem placeat praesentium rem. Adipisci debitis ea iste labore minus numquam ratione rem vitae, voluptates. Cupiditate dignissimos et exercitationem laboriosam, repellat repudiandae sint. Consequuntur corporis cupiditate ipsa modi molestiae sunt. Ab accusamus, aliquid aperiam consectetur deleniti eum exercitationem explicabo ipsa nam, nisi quo quos tempore tenetur. Magni.'
		//     }
		// ]
	}

	public createExam(): void {
		this.appService.goToPageWithParams(RoutingConstants.EXAM_FORM_EXT, {subjectId: this.currentSubjectId});
	}

	public updateSubject(exam: Exam): void {
		this.appService.goToPageWithParams(RoutingConstants.EXAM_FORM_EXT, {
			subjectId: this.currentSubjectId,
			examId: exam.examId,
			examName: exam.examName,
			examDescription: exam.examDescription
		});
	}

	public deleteSubject(exam: Exam): void {
		this.service.deleteExam({examId: exam.examId}, (res) => {
			if (!res.errorId) {
				this.getExams(this.currentSubjectId);
				// TODO: все success и тд в константы
				return this.modalService.notify('success', 'MODAL.SUCCESS', res.errorMessage);
			}

			this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage);
		});
	}

	public goToTaskPage(exam: Exam): void {
		this.appService.goToPageWithParams(RoutingConstants.TASK_EXT, {examId: exam.examId});
	}

	ngOnDestroy() {
		this.routeParamsSub.unsubscribe();
	}

}
