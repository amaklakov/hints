import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApplicationService} from '../../../../services/application.service';
import {HttpClientService} from '../../../../services/http-client.service';
import {Question} from './question.interface';
import {Subscription} from 'rxjs/index';
import {QuestionService} from './question.service';

@Component({
	selector: 'question',
	templateUrl: 'question.component.html'
})

export class QuestionComponent implements OnInit, OnDestroy {
	public routeParamsSub: Subscription;
	public currentTaskId: string;
	public question: Question;

	constructor(private appService: ApplicationService,
	            private httpClient: HttpClientService,
	            private service: QuestionService) {
	}

	ngOnInit() {
		this.routeParamsSub = this.appService.getParams((params: { taskId: string }) => {
			this.currentTaskId = params.taskId;

			this.getQuestion(this.currentTaskId);
		});
	}

	public getQuestion(taskId: string) {
		this.service.getQuestion({taskId: taskId}, (res: any) => {
			this.question = res.content;
		});

		// this.question = {
		//     taskId: taskId,
		//     questionId: '6667',
		//     questionText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur facilis harum iure laudantium perspiciatis quaerat reiciendis soluta tenetur veniam. Aliquam aperiam deserunt ipsum libero obcaecati officiis quis quisquam sed soluta tenetur. Aliquam architecto earum eligendi eveniet excepturi iure magnam, odit officia, possimus quisquam quo repellat ullam unde. Animi consequuntur, cumque ducimus enim hic impedit itaque molestiae quo, ratione tempora ullam vel voluptas. Amet hic maiores officia saepe voluptates. Commodi cumque delectus eum in ipsam, ipsum natus nesciunt numquam optio quasi quo sed similique temporibus ullam, ut veniam, voluptates. Alias consequatur cupiditate deserunt doloremque dolorum et eum excepturi exercitationem harum molestiae neque numquam odit officia officiis possimus quas qui quibusdam quidem, quod reiciendis sunt tenetur, vel vero. Autem doloribus iusto reiciendis? Aliquam amet assumenda at consequatur corporis doloremque eligendi, enim esse eveniet facilis fugit illo impedit ipsam ipsum laborum molestias nam, necessitatibus nihil perspiciatis placeat porro praesentium quae qui reiciendis repellat similique tempora tenetur vero voluptas voluptatem. A at doloremque dolores eaque earum eius eligendi enim esse, eveniet, explicabo facere fuga hic, id illum ipsa ipsam itaque nam nihil perspiciatis placeat provident quam quibusdam sit sunt tempora totam ullam? Est explicabo illo maiores, minima nemo nihil nostrum numquam porro possimus quis quod sint temporibus. Ab blanditiis error facilis molestiae nemo recusandae repellat suscipit. Doloremque doloribus ipsa magni perspiciatis quas rerum soluta temporibus vel veniam vero? Ab ad aliquam aspernatur beatae commodi consequatur corporis culpa debitis doloribus, dolorum ea enim error eveniet fugiat illo incidunt iste labore magni molestiae necessitatibus nemo nesciunt nobis officia possimus praesentium quis saepe soluta tenetur ullam ut. Aspernatur dolores enim porro. Aperiam consequatur cupiditate earum itaque molestiae quis rem rerum, vero voluptatibus? Commodi cum minus soluta tempora. Animi asperiores aut, debitis delectus deserunt dignissimos error esse est explicabo illo, impedit in incidunt ipsa laboriosam libero magnam minima mollitia necessitatibus nesciunt, nobis non nulla odit officia officiis omnis possimus provident qui reiciendis repellat rerum sed suscipit ullam unde velit veniam vitae voluptatum? Autem iure non praesentium reprehenderit. Adipisci aliquam at cupiditate deserunt, earum enim fugiat impedit labore minus nemo non optio quis, saepe sint, temporibus ut vero? Aspernatur, at delectus deleniti doloremque error eum facere facilis ipsum, iusto minima minus modi perspiciatis possimus quaerat quam quis temporibus totam unde vero voluptates! Nam quibusdam quis soluta suscipit voluptas. Accusantium, ipsam, repudiandae! Atque consequatur, cumque delectus ducimus eos et illo nemo nobis officia, quibusdam repellendus sapiente similique tenetur. Aliquid doloremque dolores facilis molestiae neque perferendis quis tempora tempore ut voluptatem. Beatae dolorum laborum optio ut vitae. Aliquid consectetur deleniti dignissimos eius eum mollitia nobis quaerat, quisquam velit voluptates. Alias commodi consequuntur dolore hic ipsa laudantium magni nisi optio quos voluptatum? Aperiam asperiores, aspernatur consequatur, ducimus eos et explicabo id ipsum iure minus molestias nam necessitatibus perferendis provident sed similique vero! Aspernatur dolorum nulla sequi. Ad autem commodi consequatur culpa delectus doloribus earum error est expedita fugiat ipsum, iure maiores maxime minima nam natus nemo nostrum numquam perspiciatis possimus quae quas rem repellendus sint sunt, suscipit vero? Aliquam amet architecto autem eius est eveniet exercitationem molestiae nemo nisi quo ratione sequi, sint temporibus tenetur vitae! Animi architecto assumenda autem blanditiis cum, dignissimos distinctio, dolore, earum excepturi explicabo harum laboriosam magni nemo nesciunt obcaecati possimus quisquam saepe sint. Asperiores, corporis enim esse est molestiae possimus qui quis, recusandae saepe sunt temporibus unde, veritatis. Amet architecto beatae consectetur distinctio illo, inventore labore maiores omnis perferendis quas qui quia quisquam rerum sunt voluptatum. Accusantium aliquid distinctio dolor dolore eveniet excepturi hic ipsa iure maxime necessitatibus odio perspiciatis quibusdam sint soluta tempore ullam, unde voluptate? Ducimus nisi, quibusdam? Cum dignissimos dolor illum iste, itaque labore necessitatibus nisi odio quam quibusdam quis recusandae reiciendis repellendus saepe tempora vel velit. Architecto, delectus dolore eius expedita hic incidunt iure iusto laboriosam molestias, obcaecati odit perspiciatis placeat quia quis ut veniam voluptatem? Ad architecto aspernatur assumenda at atque beatae consectetur cupiditate dicta dolor dolorum eius, eos est excepturi expedita explicabo facere illo in ipsum iure laudantium magni maiores maxime, modi molestiae mollitia nobis non numquam odio officia officiis qui quibusdam quidem quis quo repellat soluta ut. In magni voluptate voluptatum! A aliquid asperiores autem consequatur delectus dignissimos excepturi exercitationem in ipsam ipsum iusto labore magnam minus molestias mollitia nam, nisi nostrum officia omnis placeat quas quibusdam repudiandae sapiente sequi sint soluta ut. Assumenda blanditiis consectetur consequatur consequuntur debitis eaque esse est fugit in incidunt iure laborum, magnam molestiae odio perferendis recusandae reiciendis repellat repudiandae vel voluptas? Amet aperiam assumenda delectus doloremque dolores eius eos, esse id illo labore libero mollitia nemo nisi nostrum odio possimus quidem voluptate. A accusantium aliquam aspernatur culpa cum dicta distinctio doloremque error est fugit harum id, in laudantium libero nihil officia perspiciatis quaerat quam quis sit ut veritatis voluptates! Dolore, doloremque dolores enim est illo incidunt maiores molestias necessitatibus, numquam omnis rem soluta ullam. At atque consectetur consequatur delectus distinctio ducimus error esse excepturi illo illum minus molestiae nam nisi pariatur, perspiciatis quas qui, recusandae repellat repudiandae voluptas? Aspernatur distinctio dolor esse fugit modi ut vel? A accusamus architecto eligendi incidunt officia, omnis perferendis quia vero! Cum distinctio minus, perferendis praesentium ratione repellat sed tempore tenetur vero voluptate? Alias amet at aut beatae cupiditate delectus earum eligendi enim eum fugit libero maxime minus molestiae nam odio, porro quae quas quo repellat repellendus saepe sunt tempora tenetur ullam voluptates! Aliquam distinctio doloremque error eum in, modi molestiae mollitia nihil quae, quidem, quisquam suscipit? Aspernatur assumenda blanditiis culpa delectus nostrum quo sapiente tempora temporibus. Architecto autem deserunt in ipsum iure numquam perferendis quo, tenetur! Aliquid animi architecto asperiores consequatur eveniet excepturi fugiat fugit id incidunt inventore iure libero minima molestiae nobis placeat possimus quas quidem quos reiciendis, sint tempore, unde vel veniam vero voluptatum? A ad dolor et laudantium possimus quae quaerat quos rem ut vel? Accusamus accusantium beatae commodi cupiditate eius excepturi facere fuga harum impedit inventore ipsa ipsam libero maxime modi molestias mollitia nam natus nesciunt nulla numquam odit pariatur praesentium qui quis quisquam quod similique tenetur unde, vel, velit veniam vero voluptatum?'
		// };
	}

	ngOnDestroy() {
		this.routeParamsSub.unsubscribe();
	}
}
