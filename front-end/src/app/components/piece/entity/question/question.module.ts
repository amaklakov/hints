import {NgModule} from '@angular/core';
import {QuestionComponent} from './question.component';
import {SharedModule} from '../../../../shared.module';
import {QuestionService} from './question.service';

@NgModule({
	imports: [SharedModule],
	exports: [QuestionComponent],
	declarations: [QuestionComponent],
	providers: [QuestionService]
})
export class QuestionModule {
}
