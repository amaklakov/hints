export interface Question {
	taskId: string;             // ID вопроса
	questionId: string;         // имя вопроса
	questionText: string;       // описание вопроса
}
