import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../../services/http-client.service';
import {BackEndConstants} from '../../../../constants/back-end.constants';

@Injectable()
export class QuestionService {

	constructor(private httpClient: HttpClientService) {
	}

	public getQuestion(params: any, successCallback: Function): void {
		this.httpClient.callGET(BackEndConstants.GET_QUESTION_BY_TASK_ID, successCallback, params);
	}
}
