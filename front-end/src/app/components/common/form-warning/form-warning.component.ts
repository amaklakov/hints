import {Component, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ValidationErrors} from '@angular/forms/src/directives/validators';

@Component({
	selector: 'form-warning',
	templateUrl: './form-warning.component.html'
})
export class FormWarningComponent {
	public errorMessages: Array<string>;

	@Input('errors')
	public set err(value: ValidationErrors) {
		this.checkErrors(value);
	};

	private ERRORS = {
		required: 'FIELD_MESSAGES.REQUIRED',
		pattern: 'FIELD_MESSAGES.PATTERN',
		minlength: 'FIELD_MESSAGES.MINLENGTH',
		maxlength: 'FIELD_MESSAGES.MAXLENGTH',
		custom: 'FIELD_MESSAGES.CUSTOM'
	};

	constructor(private translate: TranslateService) {
	}

	/**
	 * Проверяет ошибки
	 */
	public checkErrors(errors: ValidationErrors): void {
		if (!errors) {
			return this.errorMessages = null;
		}

		this.errorMessages = [];
		for (let error in errors) {
			this.errorMessages.push(this.getErrorText(errors[error], error));
		}
	}

	/**
	 * Получает строку с подсказкой
	 * @param error - сама ошибка
	 * @param {string} errorName - название ошибки
	 * @returns {string} - строка с подсказкой
	 */
	private getErrorText(error: any, errorName: string): string {
		if (errorName === 'minlength' || errorName === 'maxlength') {
			return this.translate.instant(this.ERRORS[errorName]) + error.requiredLength;
		}

		return this.translate.instant(this.ERRORS[errorName] ? this.ERRORS[errorName] : this.ERRORS.custom);
	}
}
