import {NgModule} from '@angular/core';
import {FormWarningComponent} from './form-warning.component';
import {SharedModule} from '../../../shared.module';

@NgModule({
	imports: [
		SharedModule
	],
	declarations: [FormWarningComponent],
	exports: [FormWarningComponent]
})
export class FormWarningModule {
}
