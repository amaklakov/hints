import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {MODAL_COLORS, MODAL_TYPES} from './help-modal';

@Component({
	selector: 'ait-modal',
	templateUrl: './modal.component.html',
	animations: [
		trigger('fadeInAnimation', [
			// route 'enter' transition
			transition(':enter', [

				// styles at start of transition
				style({opacity: 0}),

				// animation and styles at end of transition
				animate('.3s ease-out', style({opacity: 1}))
			]),
			transition(':leave', [
				// animation and styles at end of transition
				animate('.5s ease-in-out', style({
					// transition the right position to -400% which slides the content out of view
					opacity: '0'
				}))
			])
		])
	],
	host: {'[@fadeInAnimation]': ''}
})
export class ModalComponent implements OnInit {
	@Input() public type: string;
	public modalColor: string = MODAL_COLORS.NOTIFICATION;

	@Input() public title: string;
	@Input() public body: string;
	@Input() public hasFooter: boolean = false;

	@Output() public destroyComponent: EventEmitter<any> = new EventEmitter<any>();

	constructor() {
	}

	ngOnInit() {
		this.checkType();
	}

	/**
	 * Устанавливает тип модального окна
	 * и передает значения в метод для установки всех настроек модалки
	 */
	private checkType(): void {
		switch (this.type ? this.type : MODAL_TYPES.NOTIFICATION) {
			case MODAL_TYPES.ERROR:
				this.setModal(false, MODAL_COLORS.ERROR);
				break;
			case MODAL_TYPES.WARNING:
				this.setModal(false, MODAL_COLORS.WARNING);
				break;
			case MODAL_TYPES.NOTIFICATION:
				this.setModal(false, MODAL_COLORS.NOTIFICATION);
				break;
			default:
				this.type = MODAL_TYPES.NOTIFICATION;
				this.setModal(false, MODAL_COLORS.NOTIFICATION);
		}
	}

	/**
	 * Устанавливает все необходимые классы и свойства модальному окну
	 * @param {boolean} hasFooter
	 * @param {string} modalColor
	 */
	private setModal(hasFooter: boolean, modalColor: string): void {
		this.hasFooter = hasFooter;
		this.modalColor = modalColor;
	}

	/**
	 * Эмитит значение, чтобы сервис разрушил эту модалку
	 */
	public callToDestroy(): void {
		this.destroyComponent.emit(true);
	}
}

