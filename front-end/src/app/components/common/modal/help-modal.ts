export class MODAL_TYPES {
	// TODO: добавить поле CUSTOM
	public static WARNING: string = 'warning';
	public static ERROR: string = 'error';
	public static NOTIFICATION: string = 'notification';
}

export class MODAL_COLORS {
	// TODO: добавить поле CUSTOM
	public static WARNING: string = '-yellow';
	public static ERROR: string = '-red';
	public static NOTIFICATION: string = '-green';
}
