import {
	ApplicationRef,
	ComponentFactory,
	ComponentFactoryResolver,
	EmbeddedViewRef,
	Injectable,
	Injector
} from '@angular/core';
import {ModalComponent} from './modal.component';
import {Subscription} from 'rxjs/index';
import {isPresent} from '../../../util/util';

@Injectable()
export class ModalService {
	private defaultTime: number = 5000;

	constructor(private resolver: ComponentFactoryResolver,
	            private injector: Injector,
	            private appRef: ApplicationRef) {
	}

	/**
	 * Создать уведомление
	 */
	public notify(modalType: string, modalHeader: string, modalBody?: string, time?: number): void {
		this.createModal(modalType, modalHeader, modalBody, time);
	}


	private createModal(modalType: string, modalHeader: string, modalBody: string, time?: number): void {
		let modalTime: number = isPresent(time) ? time : this.defaultTime;

		// 0. Строим фабрику
		let factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(ModalComponent);

		// 1. Create a component reference from the component
		const componentRef = factory.create(this.injector);

		// Заполняем инпуты компонента необходимыми данными
		componentRef.instance.type = modalType;
		componentRef.instance.title = modalHeader;
		componentRef.instance.body = modalBody;

		// 2. Attach component to the appRef so that it's inside the ng component tree
		this.appRef.attachView(componentRef.hostView);

		// 3. Get DOM element from component
		const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

		// 4. Append DOM element to the proper place of DOM
		document.getElementById('notifications').appendChild(domElem);

		this.closeModal(componentRef, modalTime);
	}

	/**
	 * Закрываем модальное окно по времени и подписываемся на клик по крестику
	 * @param componentRef
	 * @param {number} time
	 */
	private closeModal(componentRef, time?: number): void {
		let destroySub: Subscription = componentRef.instance.destroyComponent.subscribe((res) => {
			this.destroyComponent(res, destroySub, componentRef);
		});

		if (isPresent(time) && time !== 0) {
			setTimeout(() => {
				this.destroyComponent(true, destroySub, componentRef);
			}, time);
		}
	}

	/**
	 * Метод для удаления модального окна из DOM
	 * @param {{isNeedDestroy: boolean}} res - Output() в ModalComponent
	 * @param {Subscription} sub - подписка на Output() в ModalComponent
	 * @param componentRef - ссылка на убваемый instance
	 */
	private destroyComponent(res: boolean, sub: Subscription, componentRef: any): void {
		if (res) {
			sub.unsubscribe();
			this.appRef.detachView(componentRef.hostView);
			componentRef.destroy();
		}
	}
}
