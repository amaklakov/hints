import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

	constructor() {
	}

	public getItem(key: string): string {
		return (key && key.length) ? localStorage.getItem(key) : '';
	}

	public setItem(key: string, value: any): void {
		if (key && value && key.length && value.toString().length) {
			localStorage.setItem(key, value.toString());
		} else {
			console.log(' --- LocalStorageService.setItem --- NOT VALID KEY OR VALUE');
		}
	}

	public clear(): void {
		localStorage.clear();
	}
}
