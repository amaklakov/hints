import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BackEndConstants} from '../constants/back-end.constants';
import {ModalService} from '../components/common/modal/modal.service';
import {LocalStorageService} from './local-storage.service';
import {RoutingConstants} from '../constants/routing.constants';

@Injectable()
export class HttpClientService {

	private headers: HttpHeaders = new HttpHeaders({
		'Content-Type': 'application/json'
	});

	constructor(private http: HttpClient,
	            private modalService: ModalService,
	            private localStorageService: LocalStorageService) {
	}

	/**
	 * GET-запрос
	 * @param {string} url - url запроса
	 * @param {Function} successCallback - функция-обработчик ответа
	 * @param params - параметры запроса
	 */
	public callGET(url: string, successCallback: Function, params?: any): void {
		let concatURL = BackEndConstants.COMMON + url;

		this.http.get(concatURL, {headers: this.makeHeaders(), params: params})
			.subscribe((res: { errorId: number, errorMessage: string, content: any }) => {
				if (res.errorId) {
					return this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage, 0);
				}

				successCallback(res);
			}, (err) => {
				this.modalService.notify('error', 'MODAL.ERROR', err.message, 0);
			});
	}

	/**
	 * POST - запрос
	 * @param {string} url - url запроса
	 * @param body - тело запроса
	 * @param {Function} successCallBack - функция-обработчик ответа
	 * @param params - параметры запроса
	 */
	public callPOST(url: string, body: any, successCallBack: Function, params?: any): void {
		let concatURL = BackEndConstants.COMMON + url;

		this.http.post(concatURL, body, {headers: this.makeHeaders(), params: params})
			.subscribe((res: any) => {
				if (res.errorId) {
					return this.modalService.notify('error', 'MODAL.ERROR', res.errorMessage, 0);
				}

				successCallBack(res);
			}, (err) => {
				this.modalService.notify('error', 'MODAL.ERROR', err.message, 0);
			});
	}

	/**
	 * Делает хедеры
	 */
	private makeHeaders(): HttpHeaders {
		// TODO поменять создание хедеров
		let sessionToken = this.localStorageService.getItem(RoutingConstants.SESSION_TOKEN);
		if (sessionToken) {
			return new HttpHeaders({
				'Session-Token': sessionToken,
				'Content-Type': 'application/json'
			});
		}

		return new HttpHeaders({
			'Content-Type': 'application/json'
		});
	}
}
