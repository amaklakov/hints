import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {Subscription} from 'rxjs/index';
import {ApplicationDataService} from './application-data.service';

@Injectable()
export class ApplicationService {

	constructor(private router: Router,
	            private activatedRoute: ActivatedRoute,
	            public appData: ApplicationDataService) {
	}

	/**
	 * Переход на страницу
	 * @param {string} url - url страницы, без "/"
	 */
	public goToPage(url: string): void {
		let concatURL = '/' + url;
		this.router.navigateByUrl(concatURL);
	}

	/**
	 * Переход на страницу с параметрами
	 * @param {string} url - url страницы, без "/"
	 * @param params - необходимые параметры для перехода
	 */
	public goToPageWithParams(url: string, params: any): void {
		let navigationExtras: NavigationExtras = {queryParams: params};
		let concatURL = '/' + url;

		this.router.navigate([concatURL], navigationExtras);
	}

	/**
	 * Открывает ссылку в новой вкладке
	 * @param url
	 */
	public openLinkInNewTab(url: string): void {
		window.open(url, '_blank');
	}

	/**
	 * Достать параметры маршрута
	 * @param {any} parseFunction - функция-обработчик
	 * @returns {Subscription} - подписка на параметры
	 */
	public getParams(parseFunction: any): Subscription {
		return this.activatedRoute.queryParams.subscribe(parseFunction);
	}
}
