import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {RoutingConstants} from '../constants/routing.constants';

@Injectable()
export class AuthGuardService implements CanActivate {

	constructor() {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		// return this.checkUser(route) && localStorage.getItem(RoutingConstants.SESSION_TOKEN) !== '';
		return true;
	}

	private checkUser(route: ActivatedRouteSnapshot): boolean {
		for (let role of route.data.roles) {
			if (localStorage.getItem(RoutingConstants.USER_ROLE) == role) {
				return true;
			}
		}

		return false;
	}

}
