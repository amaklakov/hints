import {Injectable} from '@angular/core';
import {LocalStorageService} from './local-storage.service';
import {RoutingConstants} from '../constants/routing.constants';

@Injectable()
export class ApplicationDataService {
	public isLogged: boolean = !!this.localStorageService.getItem(RoutingConstants.SESSION_TOKEN);

	constructor(private localStorageService: LocalStorageService) {
	}


}
