export class RoutingConstants {
	public static readonly LOGIN: string = 'login';
	public static readonly REGISTER: string = 'register';
	public static readonly WORK_PLACE: string = 'work-place';
	public static readonly NEWS: string = 'news';
	public static readonly ABOUT: string = 'about';
	public static readonly SUBJECT: string = 'subjects';
	public static readonly SUBJECT_FORM: string = 'create-subject';
	public static readonly EXAM: string = 'exams';
	public static readonly EXAM_FORM: string = 'create-exam';
	public static readonly TASK: string = 'questions';
	public static readonly TASK_FORM: string = 'create-task';
	public static readonly QUESTION: string = 'question';

	// Extended
	public static readonly SUBJECT_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.SUBJECT;
	public static readonly SUBJECT_FORM_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.SUBJECT_FORM;
	public static readonly EXAM_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.EXAM;
	public static readonly EXAM_FORM_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.EXAM_FORM;
	public static readonly TASK_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.TASK;
	public static readonly TASK_FORM_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.TASK_FORM;
	public static readonly QUESTION_EXT: string = RoutingConstants.WORK_PLACE + '/' + RoutingConstants.QUESTION;

	// SOCIAL LINKS
	public static readonly VK: string = 'https://vk.com/mav0901';
	public static readonly TELEGRAM: string = 'https://google.com';
	public static readonly YOU_TUBE: string = 'https://google.com';

	// AUTH
	public static readonly SESSION_TOKEN: string = 'session-token';
	public static readonly USER_ROLE: string = 'user-role';
	public static readonly ROLE_ADMIN: number = 0;
	public static readonly ROLE_USER: number = 1;
}
