export class BackEndConstants {
	public static readonly COMMON: string = 'http://25.73.119.118:8080/api/';

	public static readonly LOGIN: string = 'login';
	public static readonly LOGOUT: string = 'logout';
	public static readonly REGISTRATION: string = 'registration';

	public static readonly GET_SUBJECTS: string = 'getSubjects';
	public static readonly CREATE_SUBJECT: string = 'createSubject';
	public static readonly UPDATE_SUBJECT: string = 'updateSubject';
	public static readonly DELETE_SUBJECT: string = 'deleteSubject';

	public static readonly GET_EXAMS_BY_SUBJECT_ID: string = 'getExamsBySubjectId';
	public static readonly CREATE_EXAM: string = 'createExam';
	public static readonly UPDATE_EXAM: string = 'updateExam';
	public static readonly DELETE_EXAM: string = 'deleteExam';

	public static readonly GET_TASKS_BY_EXAM_ID: string = 'getTasksByExamId';
	public static readonly CREATE_TASK: string = 'createTask';
	public static readonly UPDATE_TASK: string = 'updateTask';
	public static readonly DELETE_TASK: string = 'deleteTask';
	public static readonly MARK_FAVORITE: string = 'markFavorite';

	public static readonly GET_QUESTION_BY_TASK_ID: string = 'getQuestionsByTaskId';
}
