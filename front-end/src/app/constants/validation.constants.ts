export class ValidationConstants {
	public static readonly DEBOUNCE: number = 50;

	// LOGIN
	public static readonly EMAIL_PATTERN: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
	public static readonly EMAIL_MINLENGTH: number = 8;
	public static readonly EMAIL_MAXLENGTH: number = 50;

	public static readonly PASSWORD_PATTERN: string = '^[A-Za-z0-9._%+-]{8,30}$';
	public static readonly PASSWORD_MINLENGTH: number = 8;
	public static readonly PASSWORD_MAXLENGTH: number = 30;

	// REGISTRATION
	public static readonly USERNAME_PATTERN: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
	public static readonly USERNAME_MINLENGTH: number = 3;
	public static readonly USERNAME_MAXLENGTH: number = 30;

	// SUBJECT
	public static readonly SUBJECT_NAME_MINLENGTH: number = 3;
	public static readonly SUBJECT_NAME_MAXLENGTH: number = 50;

	// EXAM
	public static readonly EXAM_NAME_MINLENGTH: number = 3;
	public static readonly EXAM_NAME_MAXLENGTH: number = 150;

	public static readonly EXAM_DESCRIPTION_MINLENGTH: number = 3;
	public static readonly EXAM_DESCRIPTION_MAXLENGTH: number = 350;

	// TASK
	public static readonly TASK_NAME_MINLENGTH: number = 3;
	public static readonly TASK_NAME_MAXLENGTH: number = 150;

	public static readonly CONTENT_MINLENGTH: number = 3;
	public static readonly CONTENT_MAXLENGTH: number = 1000;
}
