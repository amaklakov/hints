export class FrontEndConstants {
	// Languages
	static readonly RU_LANG = 'ru';
	static readonly EN_LANG = 'en';
}
