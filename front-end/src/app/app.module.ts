import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SharedModule} from './shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ApplicationService} from './services/application.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRouting} from './app.routes';
import {HttpClientService} from './services/http-client.service';
import {LoginModule} from './components/piece/login/login.module';
import {HeaderModule} from './components/piece/header/header.module';
import {FooterModule} from './components/piece/footer/footer.module';
import {LocalStorageService} from './services/local-storage.service';
import {AuthGuardService} from './services/auth-guard.service';
import {RegistrationModule} from './components/piece/registration/registration.module';
import {ApplicationDataService} from './services/application-data.service';
import {ModalService} from './components/common/modal/modal.service';
import {ModalComponent} from './components/common/modal/modal.component';
import {NewsModule} from './components/piece/news/news.module';
import {AboutModule} from './components/piece/about/about.module';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		// ANGULAR
		CommonModule,
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,

		// Shared module
		SharedModule.forRoot(),

		// For i18n
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),

		// Custom modules
		HeaderModule,
		FooterModule,
		LoginModule,
		RegistrationModule,
		NewsModule,
		AboutModule,

		// Routing
		AppRouting
	],
	providers: [
		{provide: LocationStrategy, useClass: HashLocationStrategy},
		ApplicationService,
		ApplicationDataService,
		HttpClientService,
		LocalStorageService,
		AuthGuardService,
		ModalService
	],
	entryComponents: [
		ModalComponent
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
