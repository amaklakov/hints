import {RouterModule, Routes} from '@angular/router';
import {RoutingConstants} from './constants/routing.constants';
import {ModuleWithProviders} from '@angular/core';
import {LoginComponent} from './components/piece/login/login.component';
import {RegistrationComponent} from './components/piece/registration/registration.component';
import {NewsComponent} from './components/piece/news/news.component';
import {AboutComponent} from './components/piece/about/about.component';


const appRoutes: Routes = <Routes> [
	{path: '', redirectTo: RoutingConstants.LOGIN, pathMatch: 'full'},
	{path: RoutingConstants.LOGIN, component: LoginComponent},
	{path: RoutingConstants.NEWS, component: NewsComponent},
	{path: RoutingConstants.ABOUT, component: AboutComponent},
	{path: RoutingConstants.REGISTER, component: RegistrationComponent},
	{path: RoutingConstants.WORK_PLACE, loadChildren: './components/piece/work-place/work-place.module#WorkPlaceModule'}
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes, {useHash: true});
