import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
	exports: [
		CommonModule,
		TranslateModule
	]
})

export class SharedModule {
	// Этот метод необходим для использования в app.module
	static forRoot() {
		return {
			ngModule: SharedModule,
			providers: []
		}
	}
}
