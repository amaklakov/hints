import {Component} from '@angular/core';
import {ApplicationService} from './services/application.service';
import {HttpClientService} from './services/http-client.service';
import {TranslateService} from '@ngx-translate/core';
import {FrontEndConstants} from './constants/front-end.constants';
import {Location} from '@angular/common';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss', './css/style.scss']
})
export class AppComponent {
	usingEn: boolean = false;

	constructor(private appService: ApplicationService,
	            private httpClient: HttpClientService,
	            private location: Location,
	            private translate: TranslateService) {
		// for default languages
		// this language will be used as a fallback when a translation isn't found in the current language
		translate.setDefaultLang(FrontEndConstants.RU_LANG);
		// the lang to use, if the lang isn't available, it will use the current loader to get them
		translate.use(FrontEndConstants.RU_LANG);
	}

	/**
	 * Для смены языка
	 * @param {{usingEn: boolean}} event - эмит
	 */
	public changeLanguage(event: { usingEn: boolean }): void {
		if (event.usingEn) {
			this.translate.use(FrontEndConstants.EN_LANG);
		} else {
			this.translate.use(FrontEndConstants.RU_LANG);
		}
		this.usingEn = event.usingEn;
	}
}
